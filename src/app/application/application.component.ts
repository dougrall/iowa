import { Component, OnInit } from '@angular/core';
import { EligibilityService } from '../services/eligibility.service';
import { EligibilityInformation } from '../models/models';
import { SepInformation } from '../models/SepInformation';
import { SepEvent } from '../models/SepInformation';
import { ApplicationApplicants } from '../models/models';
import { MaskedInputDirective } from 'angular2-text-mask';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map'
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import { WMSharedModule } from '../wm-shared/wm-shared.module';
import { WMError } from '../wm-shared/wm-error/wm-error';
import * as moment from 'moment-timezone';


@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit {
  private errorList: WMError[] = [];
  public eligInformation: EligibilityInformation
  public incomeMask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
  public dateMask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  constructor(public eligibilityService: EligibilityService, public router: Router) { }
  public sepEvents: Array<SepEvent>;
  public sepInformation: SepInformation;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  ngOnInit() {
    this.eligibilityService.setTitle('Step 1 - Iowa Stopgap Measure Eligibility');
    window.scroll(0, 0);
    this.eligibilityService.getSepInformation()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(result => {
        console.log('ngOnInit: result = ' + JSON.stringify(result));
        this.sepInformation = result;
        this.sepEvents = this.sepInformation.sepEvents;
        this.eligInformation = this.eligibilityService.getApplication();
        this.eligInformation.sepFlag = this.sepInformation.sepFlag;
      })
  }

  goToContactInformation() {
    if (this.checkFields()) {
      this.eligibilityService.saveApplication(this.eligInformation);
      this.router.navigateByUrl('/contact-info');
    } this.eligInformation.sepEvent

  }

  checkFields(): boolean {
    this.errorList = [];
    if (this.eligInformation.sepFlag) {
      if (this.eligInformation.sepEvent.length === 0)
        this.errorList.push(new WMError("selSepEvent", "A qualifying special enrollment event is required when applying for special enrollment."));

      if (this.eligInformation.sepEventDatetime.length === 0) {
        this.errorList.push(new WMError("txtSEPEventDate", "The date of your qualifying special enrollment event in the format  MM/DD/YYYY is required when applying for special enrollment."));
      }
      else {
        if (!moment(this.eligInformation.sepEventDatetime, 'MM/DD/YYYY', true).isValid())
          this.errorList.push(new WMError("txtSEPEventDate", "The date of your qualifying special enrollment event in the format  MM/DD/YYYY is required when applying for special enrollment."));
      }

      if (this.eligInformation.sepEffectiveDatetime.length === 0) {
        this.errorList.push(new WMError("txtSEPDesiredEffectiveDate", "A desired coverage effective date in the format MM/DD/YYYY is required when applying for special enrollment."));
      }
      else {
        if (!moment(this.eligInformation.sepEffectiveDatetime, 'MM/DD/YYYY', true).isValid())
          this.errorList.push(new WMError("txtSEPDesiredEffectiveDate", "A desired coverage effective date in the format MM/DD/YYYY is required when applying for special enrollment."));
      }

    }

    return (this.errorList.length === 0);
  }

  focusField(field: string) {
    var fieldToFocus = document.getElementById(field);
    if (fieldToFocus)
      fieldToFocus.focus();
  }

}
