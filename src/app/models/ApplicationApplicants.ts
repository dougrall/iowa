/**
 * Eligibility API
 * This API stores eligibility information gathered in the front end
 *
 * OpenAPI spec version: 0.1
 * Contact: api@wellmark.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */



export class ApplicationApplicants {
    relationship?: string;
    firstName?: string;
    middleName?: string;
    lastName?: string;
    income?: any;
    addressLine1?: string;
    addressLine2?: string;
    city?: string;
    state?: string;
    zip?: any;
    dateOfBirth?: any;
    gender?: string;
    federalIdNumber?: any;
    federalIType?: string;
    iowaResidentFlag?: boolean;
    medicareFlag?: boolean;
    medicaidFlag?: boolean;
    accessToMinimumEssentialCovgFlag?: boolean;
    differentMailingAddr?: boolean;
    differentHomeAddr?: boolean;
    incarcerationFlag?: boolean;
    usCitizenFlag?: boolean;
    eligibleImmigrationStatusFlag?: boolean;
    seekingCovgFlag?: boolean;
    employerOfferedCovgFlag?: boolean;
    cHIPFlag?: boolean;
    documentType?: string;
    documentId?: string;

}
