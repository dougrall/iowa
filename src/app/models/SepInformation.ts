
export class SepEvent {
    value: string;
    description: string;
}
export class SepInformation {
    sepEvents: Array<SepEvent>;
    sepFlag: boolean;
}
