export class KeyInfo
{
  public key:string;
  public dob:number;
  public ssn:number;
  public income:number;
  public valid: boolean;
  public fbl: number;
}