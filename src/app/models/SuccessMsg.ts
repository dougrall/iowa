/**
 * Eligibility API
 * This API stores eligibility information gathered in the front end
 *
 * OpenAPI spec version: 0.1
 * Contact: api@wellmark.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface SuccessMsg {
    succes?: boolean;

}
