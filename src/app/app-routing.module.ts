import { NgModule } from '@angular/core';
import { PreloadAllModules, Routes, RouterModule } from '@angular/router';
import { ApplicationComponent } from './application/application.component';
import { IndividualInformationComponent } from './individual-information/individual-information.component';
import { ContactInformationComponent } from './contact-information/contact-information.component';
import { ReviewApplicationComponent } from './review-application/review-application.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { DisclaimersComponent } from './disclaimers/disclaimers.component';
import { IncomeInformationComponent } from './income-information/income-information.component'
import { GlossaryComponent } from './glossary/glossary.component'
import { GlossaryEsComponent } from './glossary-es/glossary-es.component'
// import { NoContentComponent } from './no-content/no-content.component'
// import { WmUserResolve } from './wm-shared/wm-resolvers/wm-user-resolve'

const routes: Routes = [
  /*    { path: '',   redirectTo: '/home', pathMatch: 'full' },
      { path: 'home', pathMatch: 'full', component: HomeComponent }*/
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', pathMatch: 'full', component: ApplicationComponent },
  { path: 'individuals', pathMatch: 'full', component: IndividualInformationComponent },
  { path: 'contact-info', pathMatch: 'full', component: ContactInformationComponent },
  { path: 'summary', pathMatch: 'full', component: ReviewApplicationComponent },
  { path: 'confirmation', pathMatch: 'full', component: ConfirmationComponent },
  { path: 'disclaimers', pathMatch: 'full', component: DisclaimersComponent },
  { path: 'income', pathMatch: 'full', component: IncomeInformationComponent },
  { path: 'glossary', pathMatch: 'full', component: GlossaryComponent },
  { path: 'glossary-es', pathMatch: 'full', component: GlossaryEsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
  providers: [
  ]
})

export class AppRoutingModule { }
