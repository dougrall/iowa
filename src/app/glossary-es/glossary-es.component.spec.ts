import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlossaryEsComponent } from './glossary-es.component';

describe('GlossaryEsComponent', () => {
  let component: GlossaryEsComponent;
  let fixture: ComponentFixture<GlossaryEsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlossaryEsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlossaryEsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
