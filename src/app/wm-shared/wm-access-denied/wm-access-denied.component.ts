// import { Component, OnInit, OnDestroy } from '@angular/core';
// import { Subject } from 'rxjs/Subject';
// import 'rxjs/add/operator/takeUntil';

// import * as dscModels from '../../dsc/model/models';
// import { DSCService }  from '../../dsc/dsc.service';
// import { AppDataService } from '../../app-dataservice'
// import { environment } from '../../../environments/environment';

// @Component({
//   selector: 'wm-access-denied',
//   templateUrl: './wm-access-denied.component.html',
//   styleUrls: ['./wm-access-denied.component.less'],
//   providers: [ environment.dscProvider ]
// })
// export class WmAccessDeniedComponent implements OnInit, OnDestroy {

//   constructor(public dataservice: AppDataService, private dscService: DSCService) { }

//   private ngUnsubscribe: Subject<void> = new Subject<void>()
//   private mainDSC: dscModels.MainDscInfoResponse;
//   ngOnInit() {
//     //this.dataservice.setTitle("Employer Portal - Access Denied");
//     if(!this.dataservice.mainDSCUser) {
//       this.dscService.getMainDSC(this.dataservice.currentUser.organizationId)
//         .takeUntil(this.ngUnsubscribe)
//         .subscribe(user => this.setDSCUser(user));
//     }    
//     else {
//       this.setDSCUser(this.dataservice.mainDSCUser);
//     }
//   }

//   setDSCUser(dsc: dscModels.MainDscInfoResponse) {
//     this.dataservice.mainDSCUser = dsc;
//     this.mainDSC = dsc;
//   }

//   ngOnDestroy() {
//       this.ngUnsubscribe.next();
//       this.ngUnsubscribe.complete();
// 	}

// }
