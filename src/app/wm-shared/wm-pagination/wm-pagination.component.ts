import { Component, OnInit, Input, Output, EventEmitter, ContentChild } from '@angular/core';
import { WMPage } from './wm-page'

@Component({
	selector: 'wm-pagination',
	templateUrl: './wm-pagination.component.html',
	styleUrls: ['./wm-pagination.component.less']
})
export class WmPaginationComponent implements OnInit {
	@Input() numberOfRows: number;
	@Input() rowsPerPage: number;
	@Output() pageChange: EventEmitter<string> = new EventEmitter<string>();

	currentPage: number;
	totalPages: number;
	pageList: Array<WMPage>;

	constructor() {
		this.currentPage = 1;
	}

	ngOnInit() { }

	ngOnChanges() {
		this.totalPages = Math.ceil(this.numberOfRows / this.rowsPerPage);
		this.pageList = this.createPages(this.currentPage, this.rowsPerPage, this.numberOfRows);
	}

	setPrevious() {
		this.setPage((this.currentPage - 1).toString());
		this.pageList = this.createPages(this.currentPage, this.rowsPerPage, this.numberOfRows);
	}

	setNext() {
		this.setPage((this.currentPage + 1).toString());
		this.pageList = this.createPages(this.currentPage, this.rowsPerPage, this.numberOfRows);
	}

	setPage(pageNumber: string) {
		if(+pageNumber != this.currentPage) {
			this.currentPage =  +pageNumber;
			this.pageList = this.createPages(this.currentPage, this.rowsPerPage, this.numberOfRows);
			this.pageChange.emit(pageNumber);
		}
	}

	private createPages(currentPage: number, itemsPerPage: number, totalItems: number): WMPage[] {
		let pages: Array<WMPage>;
		pages = [];
		let i = 1;
		if(this.totalPages > 1) {
			if(this.totalPages <= 5 ) {
				while(i <= this.totalPages) {
					pages.push(new WMPage(i.toString()));
					i++;
				}
			}
			else {
				if(currentPage <= 4) {
					while(i <= 4) {
						pages.push(new WMPage(i.toString()));
						i++;
					}
					pages.push(new WMPage("..."));
					pages.push(new WMPage(this.totalPages.toString()));
				}
				else if(currentPage >= this.totalPages - 3) {
					pages.push(new WMPage("1"));
					pages.push(new WMPage("..."));
					let i = this.totalPages - 3;
					while(i <= this.totalPages) {
						pages.push(new WMPage(i.toString()));
						i++;
					}
				}
				else {
					let i = this.currentPage;
					pages.push(new WMPage("1"));
					pages.push(new WMPage("..."));
					for(let p = i - 1; p < i + 2; p++ ) {
						pages.push(new WMPage(p.toString()));
					}
					pages.push(new WMPage("..."));
					pages.push(new WMPage(this.totalPages.toString()));
				}
			}
		}

		return pages;
	}

	getListClass(pageLabel: string) {
		if(pageLabel === "...") {
			return "skip";
		}
		else if(pageLabel === this.currentPage.toString()) {
			return "active";
		}
		else {
			return "";
		}
	}
}
