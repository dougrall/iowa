export class WMPage {
    pageLabel: string;
    pageNumber: number;

    constructor(label: string) {
        this.pageLabel = label;
    }
}
