import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment-timezone';

@Pipe({
	name: 'wmMoment'
})
/**
 * A date pipe that uses moment.js instead of Angular2's builtin date parser.
 * This exists because browsers are inconsistent in their interpretation of ISO 8601, and moment.js exists to fix that.
 * Even then, timezones are surprisingly hard, so a plugin (moment-timezone) exists for that. We use that too.
 */
export class WmMomentPipe implements PipeTransform {
	transform(value: string, format: string = ""): string {
		if(!value || value === "") {
			return "";
		}
		else {
			return moment.utc(value).format(format);
		}
	}
}
