import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wmSsn'
})
export class WmSsnPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    value = value.toString();
    value = value.replace(new RegExp('-', 'g'), '')
    var firstThree = value.substring(0, 3);
    var middleTwo = value.substring(3,5);
    var lastFour = value.substring(5,9);
    if(args === "Full") {
      return firstThree + "-" + middleTwo + "-" + lastFour
    }
    else {
      return "XXX-XX-" + lastFour;
    }    
  }

}
