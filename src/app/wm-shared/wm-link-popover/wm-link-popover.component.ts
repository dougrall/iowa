import { Component, OnInit, ElementRef, Renderer, Input, Output } from '@angular/core';


@Component({
  selector: 'wm-link-popover',
  templateUrl: './wm-link-popover.component.html',
  styleUrls: ['./wm-link-popover.component.less']
})
export class WmLinkPopoverComponent implements OnInit {
  @Input() popOverHTML: string;
  @Input() popOverTitle: string;
  @Input() popOverPosition: string;
  @Input() popOverLinkClass: string;
  @Input() linkText: string;
  @Input() screenReaderText: string;
  constructor(private elRef: ElementRef, private rd: Renderer) { }

  ngOnInit() {
    
  }

  popupShown() {
      this.rd.invokeElementMethod(this.elRef.nativeElement.querySelector('wm-popover-container a.close'),'focus');
  }

  getLinkClass() {
    if(this.popOverLinkClass) {
      return this.popOverLinkClass;
    }
    else {
      return null;
    }
  }
}
