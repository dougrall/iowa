import { ComponentFixture, TestBed } from "@angular/core/testing";
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ElementRef } from "@angular/core";
import { Renderer } from "@angular/core";
import { WmLinkPopoverComponent } from "./wm-link-popover.component";

describe("WmLinkPopoverComponent", () => {
	let comp: WmLinkPopoverComponent;
	let fixture: ComponentFixture<WmLinkPopoverComponent>;
	let elementRefStub: any;
	let rendererStub: any;

	beforeEach(() => {
		elementRefStub = {
			nativeElement: {
				querySelector: () => ({})
			}
		};
		rendererStub = {
			invokeElementMethod: () => ({})
		};
		TestBed.configureTestingModule({
			declarations: [ WmLinkPopoverComponent ],
			schemas: [ NO_ERRORS_SCHEMA ],
			providers: [
				{ provide: ElementRef, useValue: elementRefStub },
				{ provide: Renderer, useValue: rendererStub }
			]
		});
		fixture = TestBed.createComponent(WmLinkPopoverComponent);
		comp = fixture.componentInstance;
	});

	it("can load instance", () => {
		expect(comp).toBeTruthy();
	});

});
