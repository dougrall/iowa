import { Component, OnInit, Input, Output, EventEmitter, AfterViewChecked } from '@angular/core';
import { WMError } from '../wm-error/wm-error';

@Component({
  selector: 'wm-error-list',
  templateUrl: './wm-error-list.component.html',
  styleUrls: ['./wm-error-list.component.less']
})
export class WmErrorListComponent implements OnInit, AfterViewChecked {
  @Input() errors: Array<WMError>;
  @Output() clickError: EventEmitter<string> = new EventEmitter<string>();
  @Output() addError: EventEmitter<WMError> = new EventEmitter<WMError>();
  focusSet: boolean = false;
  constructor() { }

  ngOnInit() {    
  }

  ngAfterViewChecked() {
    if(this.errors != undefined && this.errors.length > 0 && !this.focusSet ) {
      document.getElementById("divError").focus();
      this.focusSet = true;
    }
      
  }

  ngOnChanges() {
    if(this.errors) {
      for (let e of this.errors) {
        this.addError.emit(e);
      }
    } 
    this.focusSet = false;
  }

  sendToParent(fieldName:string) {
    if(fieldName.length > 0)
      this.clickError.emit(fieldName);
  }

  checkRole(fieldName: string) {
    if(fieldName.length > 0) 
      return "link"
     else
      return "";
  }

  getTabIndex(fieldName: string) {
    if(fieldName.length > 0) 
      return "0"
     else
      return null;    
  }

  getErrorClass(fieldName: string) {
    if(fieldName.length > 0) 
      return "error-link"
    else
      return "error-no-link"    
  }
}
