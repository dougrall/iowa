import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentLoaderFactory } from 'ng2-bootstrap/component-loader';
import { PositioningService } from 'ng2-bootstrap/positioning';
import { WmPopoverConfig } from './wm-popover.config';
import { WmPopoverDirective } from './wm-popover.directive';
import { WmPopoverContainerComponent } from './wm-popover-container.component';

@NgModule({
  imports: [CommonModule],
  declarations: [WmPopoverDirective, WmPopoverContainerComponent],
  exports: [WmPopoverDirective],
  entryComponents: [WmPopoverContainerComponent]
})
export class WmPopoverModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: WmPopoverModule,
      providers: [WmPopoverConfig, ComponentLoaderFactory, PositioningService]
    };
  }
}