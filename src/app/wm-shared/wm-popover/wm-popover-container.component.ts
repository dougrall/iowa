﻿import { ChangeDetectionStrategy, Input, Component } from '@angular/core';
import { WmPopoverConfig } from './wm-popover.config';
import { isBs3 } from 'ng2-bootstrap/utils/ng2-bootstrap-config';

@Component({
  selector: 'wm-popover-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  // tslint:disable-next-line
  host: {
    '[class]': '"wm-popover in popover-" + placement + " " + placement',
    '[class.show]': '!isBs3',
    role: 'tooltip',
    style: 'display:block;'
  },
  template: `
<header>
  <a href="#" class="close" (click)="wmpopdir.hide();false;">
    CLOSE
    <span class="icon-wmkCircleClose" aria-hidden="true"></span>
  </a>
</header>
<h3 class="popover-title" *ngIf="title">{{title}}</h3>
<div class="popover-content"><ng-content></ng-content></div>
    `
})
export class WmPopoverContainerComponent {
  @Input() public placement: string;
  @Input() public title: string;
  @Input() public wmpopdir: Object;

  public get isBs3(): boolean {
    return isBs3();
  }
  
  public test(t): void {
    console.log("d test 4", this.wmpopdir);
  }

  public constructor(config: WmPopoverConfig) {
    Object.assign(this, config);
  }
}