import { Component, OnInit, Input, Output, EventEmitter, AfterViewChecked } from '@angular/core';

import { WMCountry } from './wm-country';

@Component({
  selector: 'wm-country-list',
  templateUrl: './wm-country-list.component.html',
  styleUrls: ['./wm-country-list.component.less']
})
export class WmCountryListComponent implements OnInit, AfterViewChecked {
  @Input() selectedCountry: string;
  @Input() componentLabelText: string;
  @Input() errorMessage: string;
  @Output() countryChanged: EventEmitter<string> = new EventEmitter<string>();
  private countries: Array<WMCountry>

  constructor() { 
    this.errorMessage = '';
  }
  
  ngOnInit() {
    console.log(this.selectedCountry);
    this.populateCountries();
  }

  ngAfterViewChecked() {
    if(this.errorMessage.length > 0) {
      document.getElementById("selectCountry").setAttribute("aria-describedby", "spnCountryError");
    }
    else {
      document.getElementById("selectCountry").removeAttribute("aria-describedby");
    }
  }

  sendToParent(state) {
    this.countryChanged.emit(state.currentTarget.value);    
  }

  populateCountries() {
    this.countries = [];
    this.countries.push(new WMCountry("*", "Please Select"));
    this.countries.push(new WMCountry("AFR", "AFRICA"));
    this.countries.push(new WMCountry("ARG", "ARGENTINA"));
    this.countries.push(new WMCountry("ATA", "AUSTRIA"));
    this.countries.push(new WMCountry("AUS", "AUSTRALIA"));
    this.countries.push(new WMCountry("BAH", "BAHAMAS"));
    this.countries.push(new WMCountry("BEL", "BELGIUM"));
    this.countries.push(new WMCountry("BOL", "BOLIVIA"));
    this.countries.push(new WMCountry("BOS", "BOSNIA"));
    this.countries.push(new WMCountry("BRZ", "BRAZIL"));
    this.countries.push(new WMCountry("CAN", "CANADA"));
    this.countries.push(new WMCountry("CHI", "CHINA"));
    this.countries.push(new WMCountry("CLE", "CHILE"));
    this.countries.push(new WMCountry("CRA", "COSTARICA"));
    this.countries.push(new WMCountry("CRO", "CROATIA"));
    this.countries.push(new WMCountry("CZR", "CZECHREPUBLIC"));
    this.countries.push(new WMCountry("DEN", "DENMARK"));
    this.countries.push(new WMCountry("ECU", "ECUADOR"));
    this.countries.push(new WMCountry("ELS", "ELSALVADOR"));
    this.countries.push(new WMCountry("ENG", "ENGLAND"));
    this.countries.push(new WMCountry("FRA", "FRANCE"));
    this.countries.push(new WMCountry("GER", "GERMANY"));
    this.countries.push(new WMCountry("GRB", "GREATBRITAIN"));
    this.countries.push(new WMCountry("GRE", "GREECE"));
    this.countries.push(new WMCountry("GUA", "GUAM"));
    this.countries.push(new WMCountry("HOL", "HOLLAND"));
    this.countries.push(new WMCountry("HUN", "HUNGARY"));
    this.countries.push(new WMCountry("ICE", "ICELAND"));
    this.countries.push(new WMCountry("IDA", "INDIA"));
    this.countries.push(new WMCountry("IND", "INDONESIA"));
    this.countries.push(new WMCountry("IRE", "IRELAND"));
    this.countries.push(new WMCountry("IRN", "IRAN"));
    this.countries.push(new WMCountry("IRQ", "IRAQ"));
    this.countries.push(new WMCountry("ISR", "ISRAEL"));
    this.countries.push(new WMCountry("ITA", "ITALY"));
    this.countries.push(new WMCountry("JAP", "JAPAN"));
    this.countries.push(new WMCountry("KEN", "KENYA"));
    this.countries.push(new WMCountry("KOR", "KOREA"));
    this.countries.push(new WMCountry("MAL", "MALAYSIA"));
    this.countries.push(new WMCountry("MEX", "MEXICO"));
    this.countries.push(new WMCountry("NET", "NETHERLANDS"));
    this.countries.push(new WMCountry("NKR", "NORTHKOREA"));
    this.countries.push(new WMCountry("NOR", "NORWAY"));
    this.countries.push(new WMCountry("NVS", "NOVASCOTIA"));
    this.countries.push(new WMCountry("NWZ", "NEWZEALAND"));
    this.countries.push(new WMCountry("PAK", "PAKISTAN"));
    this.countries.push(new WMCountry("PAN", "PANAMA"));
    this.countries.push(new WMCountry("PER", "PERU"));
    this.countries.push(new WMCountry("PHI", "PHILIPPINES"));
    this.countries.push(new WMCountry("POL", "POLAND"));
    this.countries.push(new WMCountry("PTR", "PUERTORICO"));
    this.countries.push(new WMCountry("ROM", "ROMANIA"));
    this.countries.push(new WMCountry("RUS", "RUSSIA"));
    this.countries.push(new WMCountry("SAF", "SOUTHAFRICA"));
    this.countries.push(new WMCountry("SAM", "SOUTHAMERICA"));
    this.countries.push(new WMCountry("SCO", "SCOTLAND"));
    this.countries.push(new WMCountry("SIN", "SINGAPORE"));
    this.countries.push(new WMCountry("SKR", "SOUTHKOREA"));
    this.countries.push(new WMCountry("SMO", "SAMOA"));
    this.countries.push(new WMCountry("SPN", "SPAIN"));
    this.countries.push(new WMCountry("SVU", "SOVIETUNION"));
    this.countries.push(new WMCountry("SWE", "SWEDEN"));
    this.countries.push(new WMCountry("SWZ", "SWITZERLAND"));
    this.countries.push(new WMCountry("TAI", "TAIWAN"));
    this.countries.push(new WMCountry("THD", "THAILAND"));
    this.countries.push(new WMCountry("TKY", "TURKEY"));
    this.countries.push(new WMCountry("UAE", "UNITEDARABEMIRATES"));
    this.countries.push(new WMCountry("UKG", "UNITEDKINGDOM"));
    this.countries.push(new WMCountry("USA", "USA"));
    this.countries.push(new WMCountry("WSI", "WESTINDIES"));
    this.countries.push(new WMCountry("YUG", "YUGOSLAVIA"));   
  }



}
