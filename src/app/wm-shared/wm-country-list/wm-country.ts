export class WMCountry {
    countryName: string;
    countryValue: string;

    constructor(name: string, value: string) {
        this.countryName = name;
        this.countryValue = value;
    }
}

