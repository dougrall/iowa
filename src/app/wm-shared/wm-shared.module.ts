import { NgModule, Component, OnInit, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Router, Routes, RouterModule, ActivatedRoute } from '@angular/router';

import { WMError } from './wm-error/wm-error';
import { WmErrorListComponent } from './wm-error-list/wm-error-list.component';
import { WmPaginationComponent } from './wm-pagination/wm-pagination.component';
import { WmStateListComponent } from './wm-state-list/wm-state-list.component';
import { WmCountryListComponent } from './wm-country-list/wm-country-list.component';
import { WmAccordionComponent } from './wm-accordion/wm-accordion/wm-accordion.component';
import { WmAccordionTabComponent } from './wm-accordion/wm-accordion-tab/wm-accordion-tab.component';
import { WmPhonePipe } from './wm-phone-pipe/wm-phone.pipe';
import { WmSsnPipe } from './wm-ssn-pipe/wm-ssn.pipe';
import { WmRelationshipPipe } from './wm-relationship-pipe/wm-relationship.pipe';
import { WmGenderPipe } from './wm-gender-pipe/wm-gender.pipe';
import { WmSepPipe } from './wm-sep-pipe/wm-sep.pipe';
import { WmYesNoPipe } from './wm-yes-no-pipe/wm-yes-no.pipe';
import { WmMomentPipe } from './wm-moment-pipe/wm-moment.pipe';
import { WmTitleCasePipe } from './wm-title-case-pipe/wm-title-case.pipe';
import {WmImmigrationDocPipe} from './wm-immigrationdoc-pipe/wm-immigrationdoc.pipe';
import { Wm6digitIntToDatePipe } from './wm-6digit-int-to-date-pipe/wm-6digit-int-to-date.pipe';
import { WmLinkPopoverComponent } from './wm-link-popover/wm-link-popover.component';
import { WmPopoverModule } from '../wm-shared/wm-popover';
import { WmSpinnerComponent } from './wm-spinner/wm-spinner/wm-spinner.component';
import { WmOrderByPipe } from './wm-order-by-pipe/wm-order-by.pipe';
//import { WmAccessDeniedComponent } from './wm-access-denied/wm-access-denied.component';
import { WmUnavailableComponent } from './wm-unavailable/wm-unavailable.component';
//import { WmUserResolve } from './wm-resolvers/wm-user-resolve';
import { WmCountyListComponent } from './wm-county-list/wm-county-list.component';
import { WmStripLeadingZeroesPipe } from './wm-strip-leading-zereos-pipe/wm-strip-leading-zeroes.pipe';
import { WmUcFirstLetterPipe } from './wm-uc-first-letter-pipe/wm-uc-first-letter.pipe';

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, RouterModule, WmPopoverModule.forRoot()],
    declarations: [
        WmErrorListComponent,
        WmPaginationComponent,
        WmStateListComponent,
        WmAccordionComponent,
        WmAccordionTabComponent,
        WmPhonePipe,
        WmImmigrationDocPipe,
        WmLinkPopoverComponent,
        Wm6digitIntToDatePipe,
        WmSsnPipe,
        WmRelationshipPipe,
        WmGenderPipe,
        WmSepPipe,
        WmYesNoPipe,
        WmMomentPipe,
        WmTitleCasePipe,
        WmLinkPopoverComponent,
        WmSpinnerComponent,
        WmOrderByPipe,
        //     WmAccessDeniedComponent,
        WmUnavailableComponent,
        WmCountryListComponent,
        WmCountyListComponent,
        WmStripLeadingZeroesPipe,
        WmUcFirstLetterPipe
    ],
    exports: [
        WmErrorListComponent,
        WmPaginationComponent,
        WmStateListComponent,
        WmAccordionComponent,
        WmAccordionTabComponent,
        WmPhonePipe,
        Wm6digitIntToDatePipe,
        WmSsnPipe,
        WmSepPipe,
        WmImmigrationDocPipe,
        WmGenderPipe,
        WmRelationshipPipe,
        WmYesNoPipe,
        WmTitleCasePipe,
        RouterModule,
        WmMomentPipe,
        WmLinkPopoverComponent,
        WmSpinnerComponent,
        WmOrderByPipe,
        WmUnavailableComponent,
        //        WmAccessDeniedComponent,
        WmCountryListComponent,
        WmCountyListComponent,
        WmStripLeadingZeroesPipe,
        WmUcFirstLetterPipe
    ],
    providers: [
        //        WmUserResolve
    ]
})

export class WMSharedModule { }

