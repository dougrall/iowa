import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wmYesNo'
})
export class WmYesNoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    var returnValue = 'Not Specified';
    if (value != null) {
      if (value) {
        returnValue = 'Yes';
      }
      else {
        returnValue = 'No';
      }
    }
    return returnValue;
  }

}
