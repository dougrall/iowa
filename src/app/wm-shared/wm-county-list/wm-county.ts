export class WMCounty {
    countyName: string;
    countyValue: string;
    countyState: string;
    isFacets: boolean;

    constructor(name: string, value: string, state: string, isFacets: boolean) {
        this.countyName = name;
        this.countyValue = value;
        this.countyState = state;
        this.isFacets = isFacets;
    }
}

