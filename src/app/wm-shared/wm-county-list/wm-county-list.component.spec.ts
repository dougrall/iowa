import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WmCountyListComponent } from './wm-county-list.component';

describe('WmCountyListComponent', () => {
  let component: WmCountyListComponent;
  let fixture: ComponentFixture<WmCountyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WmCountyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WmCountyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
