import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wmTitleCase'
})
export class WmTitleCasePipe implements PipeTransform {

  transform(input:string): string{
        if (!input) {
            return '';
        } else {
            return input.replace(/\w\S*/g, (txt => txt[0].toUpperCase() + txt.substr(1).toLowerCase() ));
        }
    }

}
