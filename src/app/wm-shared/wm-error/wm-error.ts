export class WMError {
    fieldName:string;
    errorMessage:string;


  constructor(fieldName: string, errorMessage:string){
      this.fieldName = fieldName;
      this.errorMessage = errorMessage;
  }
}