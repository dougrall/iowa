/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { WmAccordionTabComponent } from './wm-accordion-tab.component';

describe('WmAccordionTabComponent', () => {
  let component: WmAccordionTabComponent;
  let fixture: ComponentFixture<WmAccordionTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WmAccordionTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WmAccordionTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
