﻿import {Component, Input, trigger, state, transition, style, animate } from '@angular/core';

@Component({
    selector: 'wm-accordion-tab',
    templateUrl: './wm-accordion-tab.component.html',
    styleUrls: ['./wm-accordion-tab.component.less'],
    animations: [
        trigger('show', [
            state('true',  style({ height: '*' })),
            state('false', style({ height: '0', paddingTop: '0', paddingBottom: '0', display: 'none' })),
            transition('* => *', animate('200ms ease-out')),
        ])
    ]
})
export class WmAccordionTabComponent {
    @Input() header: string = null;
    private show: boolean = false;
    // So we can use things like aria-controls, which depend on element ID attributes, across many accordions
    private id: string = "wm-accordion-tab-" + (Math.floor(Math.random() * 65536)).toString(16).toLowerCase();

    constructor() { }

    toggle() {
        this.show = !this.show;
    }
}
