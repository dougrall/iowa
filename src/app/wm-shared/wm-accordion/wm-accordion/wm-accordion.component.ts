import { Component, ViewChildren } from '@angular/core';
import { WmAccordionTabComponent } from "../wm-accordion-tab/wm-accordion-tab.component";

@Component({
  selector: 'wm-accordion',
  templateUrl: './wm-accordion.component.html',
  styleUrls: ['./wm-accordion.component.less']
})
export class WmAccordionComponent {
    @ViewChildren(WmAccordionTabComponent) tabs: WmAccordionTabComponent[];

    constructor() { }
}
