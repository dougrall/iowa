import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wmSep'
})
export class WmSepPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    var returnValue = 'Not Specified';
    if (value != null) {
      if (value == 1) {
        returnValue = 'Loss of qualifying health coverage as defined in 45 C.F.R. §155.420(d)(1)';
      }
      if (value == 2) {
        returnValue = 'Change in household size due to marriage, adoption, birth, divorce, legal separation, or death';
      }
      if (value == 3) {
        returnValue = 'Change in primary place of living as defined in in 45 C.F.R. §155.420(d)(7)';
      }
      if (value == 4) {
        returnValue = 'Loss of eligibility for Medicaid or the Children’s Health Insurance Program';
      }
      if (value == 5) {
        returnValue = 'Gaining membership in a federally recognized tribe or status as an Alaskan Native Claims Settlement Act Corporate shareholder';
      }
      if (value == 6) {
        returnValue = 'Leaving incarceration';
      }
      if (value == 7) {
        returnValue = 'Change in citizenship status';
      }
      if (value == 8) {
        returnValue = 'Related to domestic abuse or spousal abandonment requiring new coverage';
      }
    }
    return returnValue;
  }

}
