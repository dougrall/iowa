import { ComponentFixture, TestBed } from "@angular/core/testing";
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { WmUnavailableComponent } from "./wm-unavailable.component";


import { Router } from "@angular/router";
import { Angulartics2GoogleTagManager } from "angulartics2";

import { CoreModule } from '../../core/core.module';



describe("WmUnavailableComponent", () => {
	let comp: WmUnavailableComponent;
	let fixture: ComponentFixture<WmUnavailableComponent>;

	beforeEach(() => {
		TestBed.configureTestingModule({
			declarations: [ WmUnavailableComponent ],
			schemas: [ NO_ERRORS_SCHEMA ]
		});
		fixture = TestBed.createComponent(WmUnavailableComponent);
		comp = fixture.componentInstance;
	});

	it("can load instance", () => {
		expect(comp).toBeTruthy();
	});

});
