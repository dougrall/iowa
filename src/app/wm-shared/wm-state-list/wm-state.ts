export class WMState {
    stateAbbreviation: string;
    stateValue: string;

    constructor(abbreviation: string, value: string) {
        this.stateAbbreviation = abbreviation;
        this.stateValue = value;
    }
}

