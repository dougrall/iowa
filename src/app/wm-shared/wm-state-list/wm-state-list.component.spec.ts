/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { WmStateListComponent } from './wm-state-list.component';

describe('WmStateListComponent', () => {
  let component: WmStateListComponent;
  let fixture: ComponentFixture<WmStateListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WmStateListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WmStateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
