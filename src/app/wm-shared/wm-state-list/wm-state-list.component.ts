import { Component, OnInit, Input, Output, EventEmitter, AfterViewChecked } from '@angular/core';

import { WMState } from './wm-state';

@Component({
  selector: 'wm-state-list',
  templateUrl: './wm-state-list.component.html',
  styleUrls: ['./wm-state-list.component.less']
})
export class WmStateListComponent implements OnInit, AfterViewChecked {
  @Input() selectedState: string;
  @Input() componentLabelText: string;
  @Input() errorMessage: string;
  @Output() stateChanged: EventEmitter<string> = new EventEmitter<string>();
  states: Array<WMState>

  constructor() { 
    this.errorMessage = '';
  }
  
  ngOnInit() {
    console.log(this.selectedState);
    this.populateStates();
  }

  ngAfterViewChecked() {
    if(this.errorMessage.length > 0) {
      document.getElementById("selectState").setAttribute("aria-describedby", "spnStateError");
    }
    else {
      document.getElementById("selectState").removeAttribute("aria-describedby");
    }
  }

  sendToParent(state) {
    this.stateChanged.emit(state.currentTarget.value);    
  }

  populateStates() {
    this.states = [];
    this.states.push(new WMState("*", "Please Select"));
    this.states.push(new WMState("AL", "Alabama"));
    this.states.push(new WMState("AK", "Alaska"));
    this.states.push(new WMState("AZ", "Arizona"));
    this.states.push(new WMState("AR", "Arkansas"));
    this.states.push(new WMState("CA", "California"));
    this.states.push(new WMState("CO", "Colorado"));
    this.states.push(new WMState("CT", "Connecticut"));
    this.states.push(new WMState("DE", "Delaware"));
    this.states.push(new WMState("FL", "Florida"));
    this.states.push(new WMState("GA", "Georgia"));
    this.states.push(new WMState("HI", "Hawaii"));
    this.states.push(new WMState("ID", "Idaho"));
    this.states.push(new WMState("IL", "Illinois"));
    this.states.push(new WMState("IN", "Indiana"));
    this.states.push(new WMState("IA", "Iowa"));
    this.states.push(new WMState("KS", "Kansas"));
    this.states.push(new WMState("KY", "Kentucky"));
    this.states.push(new WMState("LA", "Louisiana"));
    this.states.push(new WMState("ME", "Maine"));
    this.states.push(new WMState("MD", "Maryland"));
    this.states.push(new WMState("MA", "Massachusetts"));
    this.states.push(new WMState("MI", "Michigan"));
    this.states.push(new WMState("MN", "Minnesota"));
    this.states.push(new WMState("MS", "Mississippi"));
    this.states.push(new WMState("MO", "Missouri"));
    this.states.push(new WMState("MT", "Montana"));
    this.states.push(new WMState("NE", "Nebraska"));
    this.states.push(new WMState("NV", "Nevada"));
    this.states.push(new WMState("NH", "New Hampshire"));
    this.states.push(new WMState("NJ", "New Jersey"));
    this.states.push(new WMState("NM", "New Mexico"));
    this.states.push(new WMState("NY", "New York"));
    this.states.push(new WMState("NC", "North Carolina"));
    this.states.push(new WMState("ND", "North Dakota"));
    this.states.push(new WMState("OH", "Ohio"));
    this.states.push(new WMState("OK", "Oklahoma"));
    this.states.push(new WMState("OR", "Oregon"));
    this.states.push(new WMState("PA", "Pennsylvania"));
    this.states.push(new WMState("RI", "Rhode Island"));
    this.states.push(new WMState("SC", "South Carolina"));
    this.states.push(new WMState("SD", "South Dakota"));
    this.states.push(new WMState("TN", "Tennessee"));
    this.states.push(new WMState("TX", "Texas"));
    this.states.push(new WMState("UT", "Utah"));
    this.states.push(new WMState("VT", "Vermont"));
    this.states.push(new WMState("VA", "Virginia"));
    this.states.push(new WMState("WA", "Washington"));
    this.states.push(new WMState("WV", "West Virginia"));
    this.states.push(new WMState("WI", "Wisconsin"));
    this.states.push(new WMState("WY", "Wyoming"));
  }



}
