import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wm6digitIntToDate'
})
export class Wm6digitIntToDatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
   
    value = value.toString();
    while(value.toString().length<6){
      value = '0' + value;
    }
    //value = value.replace(new RegExp('-', 'g'), '')
    var firstTwo = value.substring(0, 2);
    var middleTwo = value.substring(2,4);
    var lastTwo = value.substring(4);
    
    return firstTwo + "/" + middleTwo + "/" + lastTwo
        
  

  }

}
