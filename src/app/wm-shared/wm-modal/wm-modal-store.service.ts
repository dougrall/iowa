import { Injectable } from '@angular/core';

@Injectable()
export class WmModalStoreService {
  public isActive: boolean = false;
  public activeModal: any;
  private _activeModal: any;

  constructor() {
  }

  getActiveModal(): any{
    return this._activeModal;
  }

  setActiveModal(value: any){
    this._activeModal = value;
  }

}