import { NgModule, ModuleWithProviders } from '@angular/core';

import { WmModalBackdropComponent } from './wm-modal-backdrop.component';
import { WmModalDirective } from './wm-modal.component';
import { PositioningService } from 'ng2-bootstrap/positioning';
import { ComponentLoaderFactory } from 'ng2-bootstrap/component-loader';

@NgModule({
  declarations: [WmModalBackdropComponent, WmModalDirective],
  exports: [WmModalBackdropComponent, WmModalDirective],
  entryComponents: [WmModalBackdropComponent]
})
export class WmModalModule {
  public static forRoot(): ModuleWithProviders {
    return {ngModule: WmModalModule, providers: [ComponentLoaderFactory, PositioningService]};
  }
}