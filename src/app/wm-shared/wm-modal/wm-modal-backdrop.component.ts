import { Component, ElementRef, Renderer } from '@angular/core';

import { WmClassName } from './wm-modal-constants.class';
import { isBs3 } from 'ng2-bootstrap/utils/ng2-bootstrap-config';

export class WmModalBackdropOptions {
  public animate:boolean = true;

  public constructor(options:WmModalBackdropOptions) {
    Object.assign(this, options);
  }
}

/** This component will be added as background layout for modals if enabled */
@Component({
  selector: 'wm-modal-backdrop',
  template: '',
  // tslint:disable-next-line
  host: {'class': WmClassName.BACKDROP}
})
export class WmModalBackdropComponent {
  public get isAnimated():boolean {
    return this._isAnimated;
  }

  public set isAnimated(value:boolean) {
    this._isAnimated = value;
    this.renderer.setElementClass(this.element.nativeElement, `${WmClassName.FADE}`, value);
  }

  public get isShown():boolean {
    return this._isShown;
  }

  public set isShown(value:boolean) {
    this._isShown = value;
    this.renderer.setElementClass(this.element.nativeElement, `${WmClassName.IN}`, value);
    if (!isBs3()) {
      this.renderer.setElementClass(this.element.nativeElement, `${WmClassName.SHOW}`, value);
    }
  }

  public element:ElementRef;
  public renderer:Renderer;

  protected _isAnimated:boolean;
  protected _isShown:boolean = false;

  public constructor(element:ElementRef, renderer:Renderer) {
    this.element = element;
    this.renderer = renderer;
  }
}