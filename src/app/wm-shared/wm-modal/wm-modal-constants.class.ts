import { WmModalOptions } from './wm-modal-options.class';

export const wmModalConfigDefaults:WmModalOptions = {
  backdrop: true,
  keyboard: true,
  focus: true,
  show: true,
  ignoreBackdropClick: false,
  singular: true
};

export const WmClassName:any = {
  SCROLLBAR_MEASURER: 'modal-scrollbar-measure',
  BACKDROP: 'modal-backdrop',
  OPEN: 'modal-open',
  FADE: 'fade',
  IN: 'in',         // bs3
  SHOW: 'show'  // bs4
};

export const WmSelector:any = {
  DIALOG: '.modal-dialog',
  DATA_TOGGLE: '[data-toggle="modal"]',
  DATA_DISMISS: '[data-dismiss="modal"]',
  FIXED_CONTENT: '.navbar-fixed-top, .navbar-fixed-bottom, .is-fixed'
};