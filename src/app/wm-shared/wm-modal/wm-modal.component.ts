/* tslint:disable:max-file-line-count */
// todo: should we support enforce focus in?
// todo: in original bs there are was a way to prevent modal from showing
// todo: original modal had resize events

import {
  AfterViewInit,
  ComponentRef,
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  Output,
  Renderer, ViewContainerRef
} from '@angular/core';

import { document } from  'ng2-bootstrap/utils/facade/browser';

import { isBs3 } from 'ng2-bootstrap/utils/ng2-bootstrap-config';
import { Utils } from 'ng2-bootstrap/utils/utils.class';
import { WmModalBackdropComponent } from './wm-modal-backdrop.component';
import { WmModalOptions } from './wm-modal-options.class';
import { WmClassName, wmModalConfigDefaults, WmSelector } from './wm-modal-constants.class';
import { WmModalStoreService } from './wm-modal-store.service';

import { window } from 'ng2-bootstrap/utils/facade/browser';
import { ComponentLoader } from 'ng2-bootstrap/component-loader/component-loader.class';
import { ComponentLoaderFactory } from 'ng2-bootstrap/component-loader/component-loader.factory';

const TRANSITION_DURATION = 300;
const BACKDROP_TRANSITION_DURATION = 150;

/** Mark any code with directive to show it's content in modal */
@Directive({
  selector: '[wmModal]',
  exportAs: 'wm-modal'
})
export class WmModalDirective implements AfterViewInit, OnDestroy {
  /** allows to set modal configuration via element property */
  @Input()
  public set config(conf: WmModalOptions) {
    this._config = this.getConfig(conf);
  }

  public get config(): WmModalOptions {
    return this._config;
  }

  /** This event fires immediately when the `show` instance method is called. */
  @Output() public onShow: EventEmitter<WmModalDirective> = new EventEmitter<WmModalDirective>();
  /** This event is fired when the modal has been made visible to the user (will wait for CSS transitions to complete) */
  @Output() public onShown: EventEmitter<WmModalDirective> = new EventEmitter<WmModalDirective>();
  /** This event is fired immediately when the hide instance method has been called. */
  @Output() public onHide: EventEmitter<WmModalDirective> = new EventEmitter<WmModalDirective>();
  /** This event is fired when the modal has finished being hidden from the user (will wait for CSS transitions to complete). */
  @Output() public onHidden: EventEmitter<WmModalDirective> = new EventEmitter<WmModalDirective>();

  // seems like an Options
  public isAnimated: boolean = true;
  public focusBackEl: HTMLElement;

  public get isShown(): boolean {
    return this._isShown;
  }

  protected _config: WmModalOptions;
  protected _isShown: boolean = false;

  protected isBodyOverflowing: boolean = false;
  protected originalBodyPadding: number = 0;
  protected scrollbarWidth: number = 0;

  // original type was number
  protected timerHideModal: any = 0;
  protected timerRmBackDrop: any = 0;

  // constructor props
  protected _element: ElementRef;
  protected _renderer: Renderer;

  // reference to backdrop component
  protected backdrop: ComponentRef<WmModalBackdropComponent>;
  private _backdrop: ComponentLoader<WmModalBackdropComponent>;
  // todo: implement _dialog
  private _dialog: any;
  // todo: make these options?
  private _excludeFocusElements: string[] = ['header.global-header', 'footer.global-footer', 'main'];

  @HostListener('click', ['$event'])
  public onClick(event: any): void {
    if (this.config.ignoreBackdropClick || this.config.backdrop === 'static' || event.target !== this._element.nativeElement) {
      return;
    }

    this.hide(event);
  }

  // todo: consider preventing default and stopping propagation
  @HostListener('keydown.esc')
  public onEsc(): void {
    if (this.config.keyboard) {
      this.hide();
    }
  }

  public constructor(private wmModalStoreService: WmModalStoreService, _element: ElementRef, _viewContainerRef: ViewContainerRef, _renderer: Renderer, clf: ComponentLoaderFactory) {
    this._element = _element;
    this._renderer = _renderer;
    this._backdrop = clf.createLoader<WmModalBackdropComponent>(_element, _viewContainerRef, _renderer);
  }

  public ngOnDestroy(): any {
    this.config = void 0;
    if (this._isShown) {
      this._isShown = false;
      this.hideModal();
      this._backdrop.dispose();
    }
  }

  public ngAfterViewInit(): any {
    this._config = this._config || this.getConfig();
  }

  /* Public methods */

  /** Allows to manually toggle modal visibility */
  public toggle(fbel?: HTMLElement): void {
    return this._isShown ? this.hide() : this.show(fbel);
  }

  /** Allows to manually open modal */
  public show(fbel?: HTMLElement): void {
    this.focusBackEl = (fbel) ? fbel : undefined;
    this.onShow.emit(this);
    if (this._isShown) {
      return;
    }
    if (this._config.singular) {
      if (this.wmModalStoreService.getActiveModal()) {
        this.wmModalStoreService.getActiveModal().hide();
      }
      this.wmModalStoreService.setActiveModal(this);
    }
    clearTimeout(this.timerHideModal);
    clearTimeout(this.timerRmBackDrop);

    this._isShown = true;

    this.checkScrollbar();
    this.setScrollbar();

    if (document && document.body) {
      this._renderer.setElementClass(document.body, WmClassName.OPEN, true);
    }

    this.showBackdrop(() => {
      this.showElement();
    });
  }

  /** Allows to manually close modal */
  public hide(event?: Event): void {
    if (event) {
      event.preventDefault();
    }

    this.onHide.emit(this);

    // todo: add an option to prevent hiding
    if (!this._isShown) {
      return;
    }
    
    if (this._config.singular) {
      this.wmModalStoreService.setActiveModal(undefined);
    }

    clearTimeout(this.timerHideModal);
    clearTimeout(this.timerRmBackDrop);

    this._isShown = false;
    this._renderer.setElementClass(this._element.nativeElement, WmClassName.IN, false);
    if (!isBs3()) {
      this._renderer.setElementClass(this._element.nativeElement, WmClassName.SHOW, false);
    }
    // this._addClassIn = false;

    if (this.isAnimated) {
      this.timerHideModal = setTimeout(() => this.hideModal(), TRANSITION_DURATION);
    } else {
      this.hideModal();
    }
  }

  /** Private methods @internal */
  protected getConfig(config?: WmModalOptions): WmModalOptions {
    return Object.assign({}, wmModalConfigDefaults, config);
  }

  /**
   *  Show dialog
   *  @internal
   */
  protected showElement(): void {
    // todo: replace this with component loader usage
    // if (!this._element.nativeElement.parentNode || (this._element.nativeElement.parentNode.nodeType !== Node.ELEMENT_NODE)) {
    //   // don't move modals dom position
    //   if (document && document.body) {
    //     document.body.appendChild(this._element.nativeElement);
    //   }
    // }
    this._renderer.projectNodes(document.body, [this._element.nativeElement, this.backdrop.instance.element.nativeElement]);

    this._renderer.setElementAttribute(this._element.nativeElement, 'aria-hidden', 'false');
    this._renderer.setElementStyle(this._element.nativeElement, 'display', 'block');
    this._renderer.setElementProperty(this._element.nativeElement, 'scrollTop', 0);

    if (this.isAnimated) {
      Utils.reflow(this._element.nativeElement);
    }

    // this._addClassIn = true;
    this._renderer.setElementClass(this._element.nativeElement, WmClassName.IN, true);
    if (!isBs3()) {
      this._renderer.setElementClass(this._element.nativeElement, WmClassName.SHOW, true);
    }

    const transitionComplete = () => {
      if (this._config.focus) {
        this._element.nativeElement.focus();
      }
      this.preModalShown();
      this.onShown.emit(this);
    };

    if (this.isAnimated) {
      setTimeout(transitionComplete, TRANSITION_DURATION);
    } else {
      transitionComplete();
    }
  }

  /** @internal */
  protected hideModal(): void {
    this._renderer.setElementAttribute(this._element.nativeElement, 'aria-hidden', 'true');
    this._renderer.setElementStyle(this._element.nativeElement, 'display', 'none');
    this.showBackdrop(() => {
      if (document && document.body) {
        this._renderer.setElementClass(document.body, WmClassName.OPEN, false);
      }
      this.resetAdjustments();
      this.resetScrollbar();
      this.preModalHidden();
      if (this.focusBackEl) {
        if (!this.focusBackEl.matches('a, button, input, [tabindex]')) {
          this.focusBackEl.setAttribute('tabindex', '-1');
        }
        this.focusBackEl.focus();
      }
      this.onHidden.emit(this);
    });
  }
  
  protected preModalShown(): void {
    let lastFocusLink;
    function closestSingle(el,matchEl) {
      do {} while ((matchEl !== el) && (el = el.parentElement));
      return el;
    };

    this._renderer.invokeElementMethod(document.body, 'addEventListener', [
      'focus',
      (e) => {
        if (closestSingle(e.target, this._element.nativeElement)) {
          lastFocusLink = (typeof lastFocusLink === 'undefined') ? e.target : lastFocusLink;
        } else if (lastFocusLink) {
          lastFocusLink.focus();
        }
      },
      true
    ]);
    this._excludeFocusElements.forEach((el) => {
      this._renderer.setElementAttribute(document.querySelector(el), 'aria-hidden', 'true');
    });
  }
  
  protected preModalHidden(): void {
    this._renderer.invokeElementMethod(document.body, 'removeEventListener', ['focus']);
    this._excludeFocusElements.forEach((el) => {
      this._renderer.setElementAttribute(document.querySelector(el), 'aria-hidden', null);
    });
  }

  // todo: original show was calling a callback when done, but we can use promise
  /** @internal */
  protected showBackdrop(callback?: Function): void {
    if (this._isShown && this.config.backdrop && (!this.backdrop || !this.backdrop.instance.isShown)) {
      this.removeBackdrop();
      this._backdrop
        .attach(WmModalBackdropComponent)
        .to('body')
        .show({isAnimated: false});
      this.backdrop = this._backdrop._componentRef;

      if (this.isAnimated) {
        this.backdrop.instance.isAnimated = this.isAnimated;
        Utils.reflow(this.backdrop.instance.element.nativeElement);
      }

      this.backdrop.instance.isShown = true;
      if (!callback) {
        return;
      }

      if (!this.isAnimated) {
        callback();
        return;
      }

      setTimeout(callback, BACKDROP_TRANSITION_DURATION);
    } else if (!this._isShown && this.backdrop) {
      this.backdrop.instance.isShown = false;

      let callbackRemove = () => {
        this.removeBackdrop();
        if (callback) {
          callback();
        }
      };

      if (this.backdrop.instance.isAnimated) {
        this.timerRmBackDrop = setTimeout(callbackRemove, BACKDROP_TRANSITION_DURATION);
      } else {
        callbackRemove();
      }
    } else if (callback) {
      callback();
    }
  }

  /** @internal */
  protected removeBackdrop(): void {
    this._backdrop.hide();
  }

  /** Events tricks */

  // no need for it
  // protected setEscapeEvent():void {
  //   if (this._isShown && this._config.keyboard) {
  //     $(this._element).on(Event.KEYDOWN_DISMISS, (event) => {
  //       if (event.which === 27) {
  //         this.hide()
  //       }
  //     })
  //
  //   } else if (!this._isShown) {
  //     $(this._element).off(Event.KEYDOWN_DISMISS)
  //   }
  // }

  // protected setResizeEvent():void {
  // console.log(this.renderer.listenGlobal('', Event.RESIZE));
  // if (this._isShown) {
  //   $(window).on(Event.RESIZE, $.proxy(this._handleUpdate, this))
  // } else {
  //   $(window).off(Event.RESIZE)
  // }
  // }

  /** @internal */
  protected resetAdjustments(): void {
    this._renderer.setElementStyle(this._element.nativeElement, 'paddingLeft', '');
    this._renderer.setElementStyle(this._element.nativeElement, 'paddingRight', '');
  }

  /** Scroll bar tricks */
  /** @internal */
  protected checkScrollbar(): void {
    this.isBodyOverflowing = document.body.clientWidth < window.innerWidth;
    this.scrollbarWidth = this.getScrollbarWidth();
  }

  protected setScrollbar(): void {
    if (!document) {
      return;
    }

    const fixedEl = document.querySelector(WmSelector.FIXED_CONTENT);

    if (!fixedEl) {
      return;
    }

    const bodyPadding = parseInt(Utils.getStyles(fixedEl).paddingRight || 0, 10);
    this.originalBodyPadding = parseInt(document.body.style.paddingRight || 0, 10);

    if (this.isBodyOverflowing) {
      document.body.style.paddingRight = `${bodyPadding + this.scrollbarWidth}px`;
    }
  }

  protected resetScrollbar(): void {
    document.body.style.paddingRight = this.originalBodyPadding;
  }

  // thx d.walsh
  protected getScrollbarWidth(): number {
    const scrollDiv = this._renderer.createElement(document.body, 'div', void 0);
    scrollDiv.className = WmClassName.SCROLLBAR_MEASURER;
    const scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    document.body.removeChild(scrollDiv);
    return scrollbarWidth;
  }
}