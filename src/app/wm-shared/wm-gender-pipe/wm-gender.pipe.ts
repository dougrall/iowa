import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wmGender'
})
export class WmGenderPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === "M") {
      return 'Male';
    }
    else if (value === "F") {
      return 'Female';
    } else {
      return 'Unknown';
    }
  }

}
