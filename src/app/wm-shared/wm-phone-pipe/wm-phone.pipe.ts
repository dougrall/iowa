import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wmPhonePipe'
})
export class WmPhonePipe implements PipeTransform {

  transform(value: string, args?: any): any {
      if(value) {
        value = value.replace(/[()-. ]/g, '');
        var areaCode = value.substring(0,3);
        var centralOffice =  value.substring(3,6);
        var specificLine = value.substring(6);

        return "(" + areaCode + ") " + centralOffice + "-" + specificLine;
      }
      else 
        return "";
      
  }

}
