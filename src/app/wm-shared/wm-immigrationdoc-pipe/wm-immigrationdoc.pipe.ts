import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wmImmigrationDoc'
})
export class WmImmigrationDocPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    if (value == 'I551') { return 'I-551 (Permanent Resident Card)' }
    else if (value == 'I766') { return 'I-766 (Employment Authorization Card)' }
    else if (value == 'I94UnexpiredPssprt') { return 'I-94 (Arrival/Departure Record) in Unexpired Foreign Passport' }
    else if (value == 'I94') { return 'I-94 (Arrival/Departure Record)' }
    else if (value == 'UnexpiredPassport') { return 'Unexpired Foreign Passport' }
    else if (value == 'I20') { return 'I-20 (Certificate of Eligibility for Nonimmigrant (F-1) Student Status)' }
    else if (value == 'DS2019') { return 'DS2019 (Certificate of Eligibility for Exchange Visitor (J-1) Status)' }
    else if (value == 'Naturalization') { return 'Naturalization Certificate' }
    else if (value == 'Citizenship') { return 'Certificate of Citizenship' }
    else if (value == 'I571') { return 'I-571 (Refugee Travel Document)' }
    else if (value == 'I327') { return 'I-327 (Reentry Permit)' }
    else if (value == 'Visa') { return 'Machine Readable Immigration Visa' }
    else if (value == 'TempI551') { return 'Temporary I-551 Stamp' }
    else if (value == 'Other') { return 'Other' }
    else { return 'Unknowon' };


  }

}
