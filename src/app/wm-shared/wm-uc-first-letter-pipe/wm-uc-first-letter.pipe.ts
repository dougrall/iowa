import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wmUcFirstLetter'
})
export class WmUcFirstLetterPipe implements PipeTransform {

  transform(value : string): any {
    {
      if (value.length > 1) {
        return value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
      }
      else 
      { 
        return value;
      }
    }

  }
}
