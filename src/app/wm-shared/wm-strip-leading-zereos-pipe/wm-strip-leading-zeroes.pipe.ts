import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wmStripLeadingZeroes'
})
export class WmStripLeadingZeroesPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    value = value.replace(/^0+/, '');
    return value;
  }

}
