import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WmSpinnerComponent } from './wm-spinner.component';

describe('WmSpinnerComponent', () => {
  let component: WmSpinnerComponent;
  let fixture: ComponentFixture<WmSpinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WmSpinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WmSpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
