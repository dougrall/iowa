import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wmRelationship'
})
export class WmRelationshipPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === "C") {
      return 'Self';
    }
    else if (value === "S") {
      return 'Spouse';
    } else {
      return 'Dependent';
    }
  }

}
