import { Injectable } from '@angular/core';
import { Http, ConnectionBackend, Headers, RequestOptions, Request, Response, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';  //merge map contains flap map and some other stuff
import 'rxjs/add/observable/of';  //think you’ll need this too
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { SessionStorageService, SessionStorage } from 'ng2-webstorage';
import { UUID } from 'angular2-uuid';
import { environment } from '../../environments/environment';


@Injectable()
export class Wmrk_Secure_Http extends Http {
    private getJwtUrl = environment.applicationUrl + '/getJwt';
    private refreshJwtUrl = environment.applicationUrl + '/refreshJwt';

    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions, private storage: SessionStorageService) {
        super(backend, defaultOptions);
    }

    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        return super.request(url, options).catch(res => {
            return Observable.throw(res);
        });
    }

    private extractJwt(res: Response) {
        let body = res.json();
        var storage = new SessionStorageService();
        storage.store('WellmarkJwt', body);
        return body || [];
    }

    getAuthHeaders(jwtString: string): Headers {
        try {
            var authheader = new Headers();
            authheader.append("Authorization", jwtString);
            authheader.append("vnd.wellmark.operationid", UUID.UUID());
            authheader.append("vnd.wellmark.conversationid",  this.storage.retrieve('sessionId'));            
            return authheader;
        } catch (error) {
            return (error);
        }
    }

    refreshToken(): Observable<string> {
        return super.get(this.refreshJwtUrl).map(this.extractJwt);
    }

    getToken(): Observable<string> {
        try {
            var jwtString = '';
            if(!this.storage.retrieve('sessionId'))
                this.storage.store('sessionId', UUID.UUID());

            if (environment.securityMode === 'local') {
                jwtString = environment.jwtString;
            } else {
                jwtString = this.storage.retrieve('WellmarkJwt');
            }
            if (!jwtString) {
                return super.get(this.getJwtUrl).map(this.extractJwt);
            } else {
                return Observable.of(jwtString);
            }
        } catch (error) {
            return (error);
        }
    }

    clearSessionStorage() {
        this.storage.clear('WellmarkJwt');
    }

    post(url, data, options?: RequestOptionsArgs ) {
        return this.getToken()
            .flatMap(jwtString => {
                var authHeader = this.getAuthHeaders(jwtString);
                authHeader.append("Content-type", "application/json");
                if (options) {
                    options.headers = authHeader;
                    return super.post(url, data, options);
                }
                else {
                    return super.post(url, data, { headers: authHeader});
                }
            })
            .catch(error => {
                var errorBody = error.json();
                if (errorBody.name == "TokenExpiredError") {
                    return this.refreshToken().flatMap(newJwtString => {
                        var authHeader = this.getAuthHeaders(newJwtString);
                        if (options) {
                            options.headers = authHeader;
                            return super.post(url, data, options)
                                .catch(secondError => { return Observable.throw(secondError)});
                        }
                        else {
                            return super.post(url, data, { headers: authHeader })
                                .catch(secondError => { return Observable.throw(secondError) });
                        }
                    })

                }
                return Observable.throw(error);
            })

    }

    put(url, data) {
        return this.getToken()
            .flatMap(jwtString => {
                var authHeader = this.getAuthHeaders(jwtString);
                authHeader.append("Content-type", "application/json");
                return super.put(url, data, { headers: authHeader });
            })
            .catch(error => {
                var errorBody = error.json();
                if (errorBody.name == "TokenExpiredError") {
                    return this.refreshToken().flatMap(newJwtString => {
                        var authHeader = this.getAuthHeaders(newJwtString);
                        return super.put(url, data, { headers: authHeader })
                            .catch(secondError => { return Observable.throw(secondError) });
                    })

                }
                return Observable.throw(error);
            })
    }    



    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.getToken()
            .flatMap(jwtString => {
                var authHeader = this.getAuthHeaders(jwtString);
                if (options) {
                    options.headers = authHeader;
                    return super.get(url, options)
                        .catch(secondError => { return Observable.throw(secondError)});
                }
                else {
                    return super.get(url,  { headers: authHeader })
                        .catch(secondError => { return Observable.throw(secondError) });
                }
            })
            .catch(error => {
                var errorBody = error.json();
                if (errorBody.name == "TokenExpiredError") {
                    return this.refreshToken().flatMap(newJwtString => {
                        var authHeader = this.getAuthHeaders(newJwtString);
                        return super.get(url, { headers: authHeader })
                            .catch(secondError => { return Observable.throw(secondError) });
                    })

                }
                return Observable.throw(error);
            })
    }

    delete(url: string, options?: RequestOptionsArgs, body?: any): Observable<Response> {
        return this.getToken()
            .flatMap(jwtString => {
                var authHeader = this.getAuthHeaders(jwtString);
                if(body) {
                    authHeader.append('content-type','application/json');
                    return super.delete(url, { headers: authHeader, body: body });
                }                    
                else
                    return super.delete(url, { headers: authHeader });
            })
            .catch(error => {
                var errorBody = error.json();
                if (errorBody.name == "TokenExpiredError") {
                    return this.refreshToken().flatMap(newJwtString => {
                        var authHeader = this.getAuthHeaders(newJwtString);
                        return super.delete(url, { headers: authHeader })
                            .catch(secondError => { return Observable.throw(secondError) });
                    })

                }
                return Observable.throw(error);
            })
    }
}