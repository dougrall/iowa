import { Component, OnInit } from '@angular/core';
import { EligibilityService } from '../services/eligibility.service';
import { EligibilityInformation } from '../models/models';
import { ApplicationApplicants } from '../models/models';
import { MaskedInputDirective } from 'angular2-text-mask';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map'


@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit {

  constructor(public eligibilityService: EligibilityService,public router: Router) { }
  public confirmationNumber: string;
  ngOnInit() {    
    this.eligibilityService.setTitle('Application Confirmation');
    window.scroll(0, 0);
    this.confirmationNumber = this.eligibilityService.getConfirmationNumber();
    if (!this.confirmationNumber) {
      this.router.navigateByUrl('/home');
    }
  }

}
