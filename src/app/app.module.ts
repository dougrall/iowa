import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, JsonpModule, Http, BaseRequestOptions, XHRBackend, RequestOptions } from '@angular/http';
import { AppComponent } from './app.component';
import { Ng2Webstorage, SessionStorageService } from 'ng2-webstorage';
import { Wmrk_Secure_Http } from './wm-shared/wmrk_http.service'
import { WMSharedModule } from './wm-shared/wm-shared.module';
import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { WmModalStoreService } from './wm-shared/wm-modal/wm-modal-store.service';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import {EligibilityService} from './services/eligibility.service';
import { FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import {ReCaptchaModule} from 'angular2-recaptcha';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { ReviewApplicationComponent } from './review-application/review-application.component';
import { IndividualInformationComponent } from './individual-information/individual-information.component';
import { ApplicationComponent } from './application/application.component';
import { ContactInformationComponent } from './contact-information/contact-information.component';
import { DisclaimersComponent } from './disclaimers/disclaimers.component';
import { IncomeInformationComponent } from './income-information/income-information.component';
import { GlossaryComponent } from './glossary/glossary.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { GlossaryEsComponent } from './glossary-es/glossary-es.component';



export function httpFactory(backend: XHRBackend, options: RequestOptions, storage: SessionStorageService) {
  return new Wmrk_Secure_Http(backend, options, storage);
}

@NgModule({
  declarations: [
    AppComponent,
    ConfirmationComponent,
    ReviewApplicationComponent,
    IndividualInformationComponent,
    ApplicationComponent,
    ContactInformationComponent,
    DisclaimersComponent,
    IncomeInformationComponent,
    GlossaryComponent,
    ErrorPageComponent,
    GlossaryEsComponent
  ],
  imports: [
    ReCaptchaModule,
    BrowserModule,
    WMSharedModule,
    Ng2Webstorage,
    HttpModule,
    AppRoutingModule,
    CoreModule ,
    FormsModule,   
     TextMaskModule,
     NgbModule.forRoot()
  ],
  providers: [{
    provide: Wmrk_Secure_Http,
    useFactory: httpFactory,
    deps: [XHRBackend, RequestOptions, SessionStorageService]
  },
    WmModalStoreService,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    EligibilityService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
