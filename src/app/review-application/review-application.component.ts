import { Component, OnInit } from '@angular/core';
import { EligibilityService } from '../services/eligibility.service';
import { EligibilityInformation } from '../models/models';
import { ApplicationApplicants } from '../models/models';
import { MaskedInputDirective } from 'angular2-text-mask';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map'


@Component({
  selector: 'app-review-application',
  templateUrl: './review-application.component.html',
  styleUrls: ['./review-application.component.css']
})
export class ReviewApplicationComponent implements OnInit {
  public eligInformation: EligibilityInformation;
  constructor(public eligibilityService: EligibilityService, public router: Router) { }

  ngOnInit() {
    this.eligibilityService.setTitle('Step 5 - Summary');
    window.scroll(0, 0);
    this.eligInformation = this.eligibilityService.getApplication();
    if (!this.eligInformation.applicants[0].firstName) {
      this.router.navigateByUrl('/home');
    }
  }
  goToIncome() {
    this.eligibilityService.saveApplication(this.eligInformation);
    this.router.navigateByUrl('/income');
  }

  goToDisclaimers() {
    this.eligibilityService.saveApplication(this.eligInformation);
    this.router.navigateByUrl('/disclaimers');
  }
}


