import { Component, OnInit } from '@angular/core';
import { EligibilityService } from '../services/eligibility.service';
import { EligibilityInformation } from '../models/models';
import { ApplicationApplicants } from '../models/models';
import { MaskedInputDirective } from 'angular2-text-mask';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map'
import { WMError } from '../wm-shared/wm-error/wm-error';


@Component({
  selector: 'app-contact-information',
  templateUrl: './contact-information.component.html',
  styleUrls: ['./contact-information.component.css']
})
export class ContactInformationComponent implements OnInit {
  private errorList: WMError[] = [];
  public eligInformation: EligibilityInformation;
  public phoneMask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public ssnMask = [/\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public dateMask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  public incomeMask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
  public states: Array<any>;
  public preferredCommunicationMethods: Array<any>;
  public immigrationDocTypes: Array<any>;



  constructor(public eligibilityService: EligibilityService, public router: Router) { }

  ngOnInit() {
    this.eligibilityService.setTitle('Step 2 - Contact Information');
    window.scroll(0, 0);
    this.eligInformation = this.eligibilityService.getApplication();

    this.states = this.eligibilityService.populateStates();
    this.preferredCommunicationMethods = new Array<any>();
    this.preferredCommunicationMethods.push(new Object({ value: "Email", description: "Email" }));
    this.preferredCommunicationMethods.push(new Object({ value: "Phone", description: "Phone" }));
  }
  goToIndividualInformation() {
    if(this.checkFields()) {
    this.eligibilityService.saveApplication(this.eligInformation);
    this.router.navigateByUrl('/individuals');
    }
  }

  goToApplications() {
    this.eligibilityService.saveApplication(this.eligInformation);
    this.router.navigateByUrl('/home');
  }
 

  checkFields() : boolean {
    this.errorList = [];
    if(this.eligInformation.mailAddressLine1.length === 0)
      this.errorList.push(new WMError("txtSubscriberMailAddrLine1","Mailing address is required."));

    if(this.eligInformation.mailCity.length === 0)
      this.errorList.push(new WMError("txtSubscriberMailCity","A city is required for the mailing address. "));    

    if(this.eligInformation.mailState.length === 0)
      this.errorList.push(new WMError("selMailState","A state is required for the mailing address. "));      

    if(this.eligInformation.mailZip.length === 0)
      this.errorList.push(new WMError("txtSubscriberMailZip","A valid 5 or 9 digit zip code is required for the mailing address."));
    else {
      var zipPattern = /^\d{5}((?:[-\s]\d{4})|(\d{4}))?$/g;
      if (!(zipPattern.test(this.eligInformation.mailZip))) 
        this.errorList.push(new WMError("txtSubscriberMailZip","A valid 5 or 9 digit zip code is required for the mailing address."));
    }
    
    if(this.eligInformation.phoneNumber.length === 0)
      this.errorList.push(new WMError("txtSubscriberPhoneNumber", "A valid 10 digit phone number is required. " ));
    else {
      var phoneEx = /\([0-9]{3}\) [0-9]{3}-[0-9]{4}$/;
      if(!phoneEx.test(this.eligInformation.phoneNumber))
        this.errorList.push(new WMError("txtSubscriberPhoneNumber", "A valid 10 digit phone number is required. " ));
    }

    if(this.eligInformation.emailAddress !== undefined && this.eligInformation.emailAddress.length > 0) {
      var emailEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(!emailEx.test(this.eligInformation.emailAddress))
        this.errorList.push(new WMError("txtSubscriberEmail", "Please enter a valid email address." ));
    }

    return (this.errorList.length === 0);
  }

  focusField(field: string) {
    var fieldToFocus = document.getElementById(field);
    if(fieldToFocus)
        fieldToFocus.focus();
  }  

}
