import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import { FooterService } from './footer-service'
import { ILanguage } from './language'
import { environment } from '../../../environments/environment';

@Component({
  selector: 'wm-footer',
  templateUrl: './footer.component.html',
  providers: [FooterService]
})
export class FooterComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject<void>()

  constructor(private footerService: FooterService) { }
  languages: ILanguage[];
  currentYear: number;
  wellmarkURL: string;
  ngOnInit() {
    this.wellmarkURL = environment.wellmarkURL;
    this.currentYear = new Date().getFullYear();
    this.footerService.getLanguages()
            .takeUntil(this.ngUnsubscribe)
            .subscribe(
              languages => this.setLanguages(languages)
            );
  }

  ngOnDestroy() {
      this.ngUnsubscribe.next();
      this.ngUnsubscribe.complete();
	}

  setLanguages(languages) {
    //this.languages = languages.languages;
    this.languages = languages;
  }

  setClass(englishKey: string) {
    if(englishKey.toUpperCase() === 'KAREN')
      return "karen";
  }

  redirect(url: string) {
    window.open(this.wellmarkURL + url);
  }

}
