export interface ILanguage {
    englishKey: string,
    nativeText: string,
    lang: string, 
    dir: string
}

