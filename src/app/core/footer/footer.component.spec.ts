//core dependencies
import { ComponentFixture, TestBed, fakeAsync, tick } from "@angular/core/testing";
import { async } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpModule } from '@angular/http';

//component being tested
import { FooterComponent } from './footer.component';

//modules / other
import { ILanguage } from './language'
import { environment } from '../../../environments/environment';
import { CoreModule } from '../core.module'
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

//services
import { FooterService } from './footer-service'
import { ComponentFixtureAutoDetect } from '@angular/core/testing';
import { MockFooterService } from '../../unit-test-common/MockFooterService'

describe('FooterComponent', () => {
  let comp: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([]), HttpModule],
      declarations: [FooterComponent],
      providers: [{ provide: FooterService, useClass: MockFooterService }]
    });

    fixture = TestBed.createComponent(FooterComponent);
    fixture.detectChanges();
  });
  
  it('Footer should contain copyrights', fakeAsync(() => {
    fixture.componentInstance.setClass("KAREN");
    tick();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    el = compiled;
    expect(el.innerText).toContain('© 2017 Wellmark, Inc. All rights reserved.');
  }));

  it('Footer should contain copyrights with date', fakeAsync(() => {
    fixture.componentInstance.setClass("KAREN");
    fixture.componentInstance.currentYear = 2222;
    tick();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    el = compiled;
    expect(el.innerText).toContain('© 2222 Wellmark, Inc. All rights reserved.');
  }));
});
