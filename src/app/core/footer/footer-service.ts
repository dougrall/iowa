import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { ILanguage} from './language';
import {LANGUAGES } from './language-list';

@Injectable()
export class FooterService {
    private _languageUrl = 'https://wm-languages-disclaimers.azurewebsites.net/languages';

    constructor(private _http: Http) { }

    getLanguages(): Observable<ILanguage[]> {

        return Observable.of(LANGUAGES);
/*        return this._http.get(this._languageUrl)
            .map((response: Response) => <ILanguage[]> response.json())
            //.do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);*/
    }

    private handleError(error: Response) {
        // logging to the console for now
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}