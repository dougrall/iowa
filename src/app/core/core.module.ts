import { NgModule, Component, OnInit, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { WmModalModule } from '../wm-shared/wm-modal';

import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { FooterService } from './footer/footer-service'

@NgModule({
    imports: [ CommonModule, FormsModule, ReactiveFormsModule, RouterModule, WmModalModule ],
    declarations: [
        NavbarComponent,
        FooterComponent      
    ],
    exports: [
        NavbarComponent,
        FooterComponent
    ],
    providers: [    
        FooterService
    ]    
})

export class CoreModule { }

