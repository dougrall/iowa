export interface NavBarElement
{
    text?: string;
    image?: string;
    url: string;
}