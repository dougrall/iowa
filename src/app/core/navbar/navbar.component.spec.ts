//core dependencies
import {
  AfterViewInit,
  ComponentRef,
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  Output,
  Renderer, ViewContainerRef
} from '@angular/core';
import { async, inject, ComponentFixture, TestBed } from "@angular/core/testing";
import { Router } from '@angular/router';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ModalModule } from 'ng2-bootstrap/modal';
import { HttpModule, Http } from '@angular/http';
import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';

//component being tested
import { NavbarComponent } from './navbar.component';

//modules / other
import { NAVBAR } from './mock-navbar';
import { NavBarService } from './navbar.service';
import { NavBarElement } from './navbar-element';
import { NavRouteGuard } from './navbar.guard';
import { CoreModule } from '../core.module'
import { WmModalModule } from '../../wm-shared/wm-modal/wm-modal.module';
import { environment } from '../../../environments/environment';
import { document } from  'ng2-bootstrap/utils/facade/browser';
import { isBs3 } from 'ng2-bootstrap/utils/ng2-bootstrap-config';
import { Utils } from 'ng2-bootstrap/utils/utils.class';
import { WmModalBackdropComponent } from '../../wm-shared/wm-modal/wm-modal-backdrop.component';
import { WmModalOptions } from '../../wm-shared/wm-modal/wm-modal-options.class';
import { WmClassName, wmModalConfigDefaults, WmSelector } from '../../wm-shared/wm-modal/wm-modal-constants.class';
import { window } from 'ng2-bootstrap/utils/facade/browser';
import { ComponentLoader } from 'ng2-bootstrap/component-loader/component-loader.class';
import { ComponentLoaderFactory } from 'ng2-bootstrap/component-loader/component-loader.factory';
import { WmModalDirective } from '../../wm-shared/wm-modal/wm-modal.component';

//services
import { WmModalStoreService } from '../../wm-shared/wm-modal/wm-modal-store.service';

describe('NavbarComponent test', () => {
  let comp: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  let navMock: any[] = NAVBAR;

  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([]), ModalModule.forRoot(), WmModalModule],
      declarations: [NavbarComponent],
      providers: [
        { provide: WmModalStoreService, useClass: WmModalStoreService },
        { provide: NavBarService, useClass: NAVBAR },
        { provide: NavRouteGuard, use: NavRouteGuard }
      ]
    }).createComponent(NavbarComponent);
    
    fixture.detectChanges();
    comp = fixture.componentInstance;
  });


  it('Nav should not be null', () => {
    const navTest = fixture.debugElement.query(By.directive(WmModalDirective));
    const navTest1 = navTest.injector.get(WmModalDirective) as WmModalDirective;

    expect(navTest1).toBeTruthy();
  });

  it('nav config not null', () => {
    const navTest = fixture.debugElement.query(By.directive(WmModalDirective));
    const navTest2 = navTest.injector.get(WmModalDirective) as WmModalDirective;

    expect(navTest2.config).toBeTruthy();
  });

  it('test is getNavBar() gets called', () => {

    spyOn(comp, 'getNavBar');

    const navTest = fixture.debugElement.query(By.directive(WmModalDirective));
    const navTest3 = navTest.injector.get(WmModalDirective) as WmModalDirective;

    comp.getNavBar();
    expect(comp.getNavBar).toHaveBeenCalled();
  });
});
