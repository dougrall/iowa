import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { NavBarService } from './navbar.service';

@Injectable()
export class NavRouteGuard implements CanActivate
{
    constructor(private navbarService: NavBarService) { }

    canActivate()
    {
        console.log('navbar.guard canActivate()');
        return false;
    }
}