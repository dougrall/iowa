import { Injectable } from '@angular/core';
import { NavBarElement } from './navbar-element';
import { NAVBAR } from './mock-navbar';

@Injectable()
export class NavBarService
{
    getNavBar(): Promise<NavBarElement[]>
    {
        return Promise.resolve(NAVBAR);
    }
}