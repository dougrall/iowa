import { NavBarElement } from './navbar-element';
import { environment } from '../../../environments/environment';

export const NAVBAR: NavBarElement[] = 
[
    <NavBarElement>{
        text: "Home",
        url: "./home"
    }
    // ,
    // <NavBarElement>{
    //     text: "eBilling",
    //     url: "./e-billing"
    // },
    // <NavBarElement>{
    //     text: "Reports",
    //     url: "./reports"
    // },
    // <NavBarElement>{
    //     text: "Resources",
    //     url: "./resources"
    // },
    // <NavBarElement>{
    //     text: "Administrative",
    //     url: "./administrative"
    // },
    // <NavBarElement>{
    //     text: "My Profile",
    //     image: "icon-user",
    //     url: "./administrative/user/profile"
    // },
    // <NavBarElement>{
    //     text: "Log Out",
    //     url: "./#/administrative/user/logout"
    // }
];
