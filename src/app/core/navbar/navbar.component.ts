import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NavBarService } from './navbar.service';
import { NavBarElement } from './navbar-element';
import { NavRouteGuard } from './navbar.guard';

@Component({
  selector: 'wm-navbar',
  templateUrl: './navbar.component.html',
  providers: [NavBarService, NavRouteGuard]
})
export class NavbarComponent implements OnInit {
   
    ngOnInit()
    {
    }

}
