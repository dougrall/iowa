import { Component, OnInit } from '@angular/core';
import { EligibilityService } from '../services/eligibility.service';
import { EligibilityInformation } from '../models/models';
import { ApplicationApplicants } from '../models/models';
import { MaskedInputDirective } from 'angular2-text-mask';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map'
import { WMError } from '../wm-shared/wm-error/wm-error';
import * as moment from 'moment-timezone';


@Component({
  selector: 'app-individual-information',
  templateUrl: './individual-information.component.html',
  styleUrls: ['./individual-information.component.css']
})
export class IndividualInformationComponent implements OnInit {
  private errorList: WMError[] = [];
  public eligInformation: EligibilityInformation;
  constructor(public eligibilityService: EligibilityService, public router: Router) { }
  public phoneMask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public ssnMask = [/\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public federalIdMask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
  public dateMask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  public incomeMask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
  public encryptedString: String;
  public dobFromForm: string;
  public ssnFromForm: string;
  public valid: Boolean = false;
  public userInfoList: Array<ApplicationApplicants>;
  public subscriber: ApplicationApplicants;
  public relationships: Array<any>;
  public immigrationDocTypes: Array<any>;
  public genders: Array<any>;
  public states: Array<any>;
  public differentMailingAddr: Boolean = false;
  public captchaResponse: string = "";


  ngOnInit() {
    this.eligibilityService.setTitle('Step 3 - Household Information');
    window.scroll(0, 0);
    this.eligInformation = this.eligibilityService.getApplication();
    if (!this.eligInformation.mailAddressLine1) {
      this.router.navigateByUrl('/home');
    }
    this.initialize();
  }
  goToContactInformation() {
    this.eligibilityService.saveApplication(this.eligInformation);
    this.router.navigateByUrl('/contact-info');
  }

  goToIncome() {
    if (this.checkFields()) {
      this.eligibilityService.saveApplication(this.eligInformation);
      this.router.navigateByUrl('/income');
    }
  }

  getRelationships() {
    return this.relationships;
  }


  initialize() {
    this.subscriber = this.eligInformation.applicants[0];
    this.relationships = new Array<any>();
    this.relationships.push(new Object({ value: 'S', description: 'Spouse' }));
    this.relationships.push(new Object({ value: 'D', description: 'Dependent' }));
    this.genders = new Array<any>();
    this.genders.push(new Object({ value: 'M', description: 'Male' }));
    this.genders.push(new Object({ value: 'F', description: 'Female' }));
    this.immigrationDocTypes = new Array<any>();
    this.immigrationDocTypes.push(new Object({ value: 'I551', description: 'I-551 (Permanent Resident Card)' }));
    this.immigrationDocTypes.push(new Object({ value: 'I766', description: 'I-766 (Employment Authorization Card)' }));
    this.immigrationDocTypes.push(new Object({ value: 'I94UnexpiredPssprt', description: 'I-94 (Arrival/Departure Record) in Unexpired Foreign Passport' }));
    this.immigrationDocTypes.push(new Object({ value: 'I94', description: 'I-94 (Arrival/Departure Record)' }));
    this.immigrationDocTypes.push(new Object({ value: 'UnexpiredPassport', description: 'Unexpired Foreign Passport' }));
    this.immigrationDocTypes.push(new Object({ value: 'I20', description: 'I-20 (Certificate of Eligibility for Nonimmigrant (F-1) Student Status)' }));
    this.immigrationDocTypes.push(new Object({ value: 'DS2019', description: 'DS2019 (Certificate of Eligibility for Exchange Visitor (J-1) Status)' }));
    this.immigrationDocTypes.push(new Object({ value: 'Naturalization', description: 'Naturalization Certificate' }));
    this.immigrationDocTypes.push(new Object({ value: 'Citizenship', description: 'Certificate of Citizenship' }));
    this.immigrationDocTypes.push(new Object({ value: 'I571', description: 'I-571 (Refugee Travel Document)' }));
    this.immigrationDocTypes.push(new Object({ value: 'I327', description: 'I-327 (Reentry Permit)' }));
    this.immigrationDocTypes.push(new Object({ value: 'Visa', description: 'Machine Readable Immigration Visa' }));
    this.immigrationDocTypes.push(new Object({ value: 'TempI551', description: 'Temporary I-551 Stamp' }));
    this.immigrationDocTypes.push(new Object({ value: 'Other', description: 'Other' }));
    this.userInfoList = this.eligInformation.applicants;
    this.states = this.eligibilityService.populateStates();
  }


  addUserInfo() {
    let userInfo = new ApplicationApplicants();
    userInfo.firstName = '';
    userInfo.lastName = '';
    userInfo.dateOfBirth = '';
    userInfo.gender = '';
    userInfo.addressLine1 = '';
    userInfo.addressLine2 = '';
    userInfo.city = '';
    userInfo.state = '';
    userInfo.zip = '';
    userInfo.federalIdNumber = '';
    userInfo.documentType = '';
    userInfo.documentId = '';
    // userInfo.differentHomeAddr = false;
    userInfo.income = '0';
    this.eligInformation.applicants.push(userInfo);
  }

  removeUserInfo(i: number) {
    this.eligInformation.applicants.splice(i, 1);

  }
  handleCorrectCaptcha(response) {
    this.captchaResponse = response;
  }

  isLastApplicant(item: any) {
    return (item == this.eligInformation.applicants.length - 1);
  }

  checkFields(): boolean {
    this.errorList = [];
    var applicantIndex: number = 0;
    var validDOB: boolean = true;
    if (this.eligInformation.applicants.filter(a => a.relationship === "S").length > 1)
      this.errorList.push(new WMError("relationship_" + applicantIndex, "Only one family member may have the relationship type of spouse."));


    for (let a of this.eligInformation.applicants) {
      if (a.relationship == undefined)
        this.errorList.push(new WMError("relationship_" + applicantIndex, "The relationship to the head of the household is required for person " + (applicantIndex + 1) + "."));
      if (a.firstName.length === 0)
        this.errorList.push(new WMError("txtSubscriberFirstname_" + applicantIndex, "The first name is required for person " + (applicantIndex + 1) + "."));

      if (a.lastName.length === 0)
        this.errorList.push(new WMError("txtSubscriberLastname_" + applicantIndex, "The last name is required for person " + (applicantIndex + 1) + "."));

      if (a.gender.length === 0)
        this.errorList.push(new WMError("selSubscriberGender_" + applicantIndex, "A gender is required for person " + (applicantIndex + 1) + "."));

      if (a.dateOfBirth.length === 0 || !moment(a.dateOfBirth, 'MM/DD/YYYY', true).isValid()) {
        this.errorList.push(new WMError("txtSubscriberDob_" + applicantIndex, "A valid date of birth in the format MM/DD/YYYY is required for person " + (applicantIndex + 1) + "."));
        validDOB = false
      } else if (moment(a.dateOfBirth, 'MM/DD/YYYY', true) > new Date()) {
        this.errorList.push(new WMError("txtSubscriberDob_" + applicantIndex, "A valid date of birth in the format MM/DD/YYYY is required for person " + (applicantIndex + 1) + "."));
        validDOB = false
      }

      if (a.differentHomeAddr) {
        if (a.addressLine1.length === 0)
          this.errorList.push(new WMError("txtSubscriberAddrLine1_" + applicantIndex, "The physical home address is required for person " + (applicantIndex + 1) + " when it is different than the mailing address."));

        if (a.city.length === 0)
          this.errorList.push(new WMError("txtSubscriberCity_" + applicantIndex, "A city is required for person " + (applicantIndex + 1) + "'s physical home address when it is different than the mailing address."));

        if (a.state.length === 0)
          this.errorList.push(new WMError("selHomeState_" + applicantIndex, "A state is required for person " + (applicantIndex + 1) + "'s physical home address when it is different than the mailing address."));

        if (a.zip === 0)
          this.errorList.push(new WMError("txtSubscriberZip_" + applicantIndex, "A valid 5 or 9 digit zip code is required for person " + (applicantIndex + 1) + "'s physical home address when it is different than the mailing address."));
        else {
          var zipPattern = /^\d{5}((?:[-\s]\d{4})|(\d{4}))?$/g;
          if (!(zipPattern.test(a.zip)))
            this.errorList.push(new WMError("txtSubscriberZip_" + applicantIndex, "A valid 5 or 9 digit zip code is required for person " + (applicantIndex + 1) + "'s physical home address when it is different than the mailing address."));
        }

      }
      let fedId = a.federalIdNumber.replace(new RegExp('_', 'g'), '')
      if (a.seekingCovgFlag && a.usCitizenFlag && validDOB && moment(a.dateOfBirth, 'MM/DD/YYYY', true).isBefore(moment().add(-1, 'years')) && fedId.length < 9)
        this.errorList.push(new WMError("txtSubscriberSsn_" + applicantIndex, "A 9 digit Federal Identification Number is required for person " + (applicantIndex + 1) + "."));

      if (a.differentHomeAddr === undefined || a.differentHomeAddr === null) {
        this.errorList.push(new WMError("rdoDifferentHomeAddressYes_" + applicantIndex, "Please indicate if person " + (applicantIndex + 1) + " has a physical home address different than the mailing address provided."));
      }

      if (a.seekingCovgFlag === undefined) {
        this.errorList.push(new WMError("rdoseekingCoverageFlagYes_" + applicantIndex, "Please indicate if person " + (applicantIndex + 1) + " is seeking coverage through an Iowa Stopgap Measure program."));
      }


      if (a.seekingCovgFlag) {
        if (a.usCitizenFlag === undefined) {
          this.errorList.push(new WMError("rdousCitizenFlagYes_" + applicantIndex, "Please indicate if person " + (applicantIndex + 1) + " is a U.S. Citizen or U.S. National."));
        }

        if (a.iowaResidentFlag === undefined)
          this.errorList.push(new WMError("rdoiowaResidentFlagYes_" + applicantIndex, "Please indicate if person " + (applicantIndex + 1) + " is a resident of Iowa."));

        if (a.incarcerationFlag === undefined)
          this.errorList.push(new WMError("rdoincarcerationFlagYes_" + applicantIndex, "Please indicate if person " + (applicantIndex + 1) + " will be incarcerated."));

        if (a.accessToMinimumEssentialCovgFlag === undefined)
          this.errorList.push(new WMError("rdoaccessToMinimumEssentialCovgFlagYes_" + applicantIndex, "Please indicate if person " + (applicantIndex + 1) + " will have access to minimal essential coverage."));

        if (a.medicareFlag === undefined)
          this.errorList.push(new WMError("rdomedicareFlagYes_" + applicantIndex, "Please indicate if person " + (applicantIndex + 1) + " will be enrolled in Medicare."));

        if (a.medicaidFlag === undefined)
          this.errorList.push(new WMError("rdomedicaidFlagYes_" + applicantIndex, "Please indicate if person " + (applicantIndex + 1) + " will be enrolled in Medicaid."));

        if (a.cHIPFlag === undefined)
          this.errorList.push(new WMError("rdochipFlagYes_" + applicantIndex, "Please indicate if person " + (applicantIndex + 1) + " will be enrolled in a Children's Health Insurance Plan."));

        if (a.usCitizenFlag != true) {
          if (a.usCitizenFlag != undefined) {
            if (a.eligibleImmigrationStatusFlag === undefined)
              this.errorList.push(new WMError("rdoEligibleImmigrationYes_" + applicantIndex, "Please indicate if person " + (applicantIndex + 1) + " has eligible immigration status."));

            if (a.eligibleImmigrationStatusFlag) {
              if (a.documentType.length === 0)
                this.errorList.push(new WMError("selDocType_" + applicantIndex, "An immigration document type is required for person " + (applicantIndex + 1) + "."));

              if (a.documentId.length === 0)
                this.errorList.push(new WMError("txtDocumentId_" + applicantIndex, "An immigration document ID is required for person " + (applicantIndex + 1) + "."));
            }
          }
        }
      }

      applicantIndex++;
    }

    if (this.errorList.length == 0) {
      if (this.eligInformation.applicants.filter(a => a.seekingCovgFlag).length === 0)
        this.errorList.push(new WMError("", "At least one family member must be seeking coverage to complete this eligibility verification request."));
    }

    return (this.errorList.length === 0);
  }

  focusField(field: string) {
    var fieldToFocus = document.getElementById(field);
    if (fieldToFocus)
      fieldToFocus.focus();
  }


}
