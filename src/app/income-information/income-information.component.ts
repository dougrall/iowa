import { Component, OnInit } from '@angular/core';
import { EligibilityService } from '../services/eligibility.service';
import { Router } from '@angular/router';
import { EligibilityInformation } from '../models/models';
import { ApplicationApplicants } from '../models/models';
import { MaskedInputDirective } from 'angular2-text-mask';
import { WMError } from '../wm-shared/wm-error/wm-error';

@Component({
  selector: 'app-income-information',
  templateUrl: './income-information.component.html',
  styleUrls: ['./income-information.component.css']
})
export class IncomeInformationComponent implements OnInit {

  constructor(public eligibilityService: EligibilityService, public router: Router) { }
  public eligInformation: EligibilityInformation;
  public incomeMask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
  private errorList: WMError[] = [];
  ngOnInit() {
    this.eligibilityService.setTitle('Step 4 - Household Income');
    window.scroll(0, 0);
    this.eligInformation = this.eligibilityService.getApplication();
    if (!this.eligInformation.applicants[0].firstName){
      this.router.navigateByUrl('/home');
    }
    //this.initialize();
  }

  goToIndividuals() {
    this.eligibilityService.saveApplication(this.eligInformation);
    this.router.navigateByUrl('/individuals');
  }
  checkFields() {
    this.errorList = [];
    for (let a of this.eligInformation.applicants) {
      if (a.income.length > 0)
        a.income = a.income.replace(new RegExp('_', 'g'), '')
    }
    return (this.errorList.length === 0);;
  }

 incomeSelect(event){
 //   console.log(JSON.stringify(event));
    event.srcElement.select();
  }

  goToReview() {
    if (this.checkFields()) {
      this.eligibilityService.saveApplication(this.eligInformation);
      this.router.navigateByUrl('/summary');
    }
  }

}
