import { Component } from '@angular/core';
import {MaskedInputDirective} from 'angular2-text-mask'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']  
})
export class AppComponent {
  title = 'Elig Verify';
}
