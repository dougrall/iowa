import { Component, OnInit } from '@angular/core';
import { EligibilityService } from '../services/eligibility.service';

@Component({
  selector: 'app-glossary',
  templateUrl: './glossary.component.html',
  styleUrls: ['./glossary.component.css']
})
export class GlossaryComponent implements OnInit {

  constructor(public eligibilityService: EligibilityService) { }

  ngOnInit() {
    this.eligibilityService.setTitle('Glossary');
    window.scroll(0, 0);
  }

}
