import { Component, OnInit } from '@angular/core';
import { EligibilityService } from '../services/eligibility.service';
import { EligibilityInformation } from '../models/models';
import { ApplicationApplicants } from '../models/models';
import { MaskedInputDirective } from 'angular2-text-mask';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map'
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import { WMError } from '../wm-shared/wm-error/wm-error';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-disclaimers',
  templateUrl: './disclaimers.component.html',
  styleUrls: ['./disclaimers.component.css']
})
export class DisclaimersComponent implements OnInit {
  private errorList: WMError[] = [];
  public eligInformation: EligibilityInformation;
  public captchaResponse: string = '';
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  public submitting: boolean=false;
  public recaptchaId: string;
  constructor(public eligibilityService: EligibilityService, public router: Router) { }

  ngOnInit() {
    this.recaptchaId = environment.recaptchaId;
    this.eligibilityService.setTitle('Step 6 - Disclaimers');
    window.scroll(0, 0);
    this.eligInformation = this.eligibilityService.getApplication();
    if (!this.eligInformation.applicants[0].firstName) {
      this.router.navigateByUrl('/home');
    }
    //this.initialize();
  }
  goToSummary() {
    this.eligibilityService.saveApplication(this.eligInformation);
    this.router.navigateByUrl('/summary');
  }

  goToConfirmation() {
    if (this.checkFields()) {
      this.submitting = true;
      this.eligibilityService.saveApplication(this.eligInformation);
      this.eligibilityService.submitApplication(this.eligInformation, this.captchaResponse)
        .takeUntil(this.ngUnsubscribe)
        .subscribe(
        response => {
          console.log('response = ' + JSON.stringify(response));
          this.router.navigateByUrl('/confirmation');
        }
        , error => {
          this.errorList.push(new WMError("", "An error occurred attempting to submit your application."));
        }
        )
    }
  }

  handleCorrectCaptcha(response) {
    this.captchaResponse = response;
    console.log(response);
  }

  checkFields(): boolean {
    this.errorList = [];
    if (this.eligInformation.consentDisclosureFlag === undefined || (this.eligInformation.consentDisclosureFlag !== undefined && !this.eligInformation.consentDisclosureFlag))
      this.errorList.push(new WMError("rdoconsentDisclaimersFlagYes", "You must agree to having read the final disclosures before you can submit this eligibility verification request."));

    if (this.eligInformation.consentDisclosureFlag !== undefined && this.eligInformation.consentDisclosureFlag) {
      if (this.captchaResponse.length === 0)
        this.errorList.push(new WMError("recaptcha", "Please check that you are not a robot to indicate you have completed this eligiblity verification request."));
    }

    if (this.eligInformation.eSignature.length === 0)
      this.errorList.push(new WMError("txtESignature", "Your electronic signature is required to submit this eligibility verification request."));

    return (this.errorList.length === 0)
  }

  focusField(field: string) {
    var fieldToFocus = document.getElementById(field);
    if (fieldToFocus)
      fieldToFocus.focus();
  }
}
