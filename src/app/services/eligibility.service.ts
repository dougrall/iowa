import { Inject, Injectable, Optional } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { RequestMethod, RequestOptions, RequestOptionsArgs } from '@angular/http';
import { Response, ResponseContentType } from '@angular/http';
import { BrowserModule, Title } from '@angular/platform-browser';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

import * as models from '../models/models';
import { SepInformation } from '../models/SepInformation';
import { environment } from '../../environments/environment';

@Injectable()
export class EligibilityService {

    protected basePath = 'http://localhost/eligibility/api/v1';
    public defaultHeaders: Headers = new Headers();
    public eligInformation: models.EligibilityInformation;
    public subscriber: models.ApplicationApplicants;
    public states: Array<any>;
    public sepInformation: SepInformation;
    public confirmationNumber: string;
    private ngUnsubscribe: Subject<void> = new Subject<void>();

    constructor(protected http: Http, private titleService: Title) {

    }

    public saveApplication(eligInformation: models.EligibilityInformation) {
        this.eligInformation = eligInformation;
    }

    public setTitle(title: string) {
        this.titleService.setTitle(title);
    }

    public getSepInformation(): Observable<SepInformation> {
        if (this.sepInformation == null) {
            var requestHeaders = new Headers();
            requestHeaders.append('content-type', 'application/json');
            return this.http.get(environment.sepInfoAoiUrl, { headers: requestHeaders })
                .takeUntil(this.ngUnsubscribe)
                .map((response: Response) => {
                    if (response.status != 200) {
                        console.log("eligibilityService.ts: error response =" + JSON.stringify(response))
                        return undefined;
                    } else {
                        console.log("eligibilityService.ts: good response =" + JSON.stringify(response))
                        return response.json() || {};
                    }
                });
        } else {
            return Observable.of(this.sepInformation);
        }

    }

    public getApplication(): models.EligibilityInformation {
        if (this.eligInformation == null) {
            this.subscriber = new models.ApplicationApplicants();
            this.subscriber.relationship = 'C';
            this.subscriber.differentMailingAddr = false;
            this.subscriber.firstName = '';
            this.subscriber.lastName = '';
            this.subscriber.gender = '';
            this.subscriber.dateOfBirth = '';
            this.subscriber.addressLine1 = '';
            this.subscriber.addressLine2 = '';
            this.subscriber.city = '';
            this.subscriber.state = '';
            this.subscriber.zip = '';
            this.subscriber.documentType = '';
            this.subscriber.documentId = '';
            this.subscriber.federalIdNumber = '';
            this.subscriber.differentHomeAddr = null;
            this.subscriber.income = '0';
            this.eligInformation = new models.EligibilityInformation();
            this.eligInformation.applicants = new Array<models.ApplicationApplicants>();
            this.eligInformation.applicants.push(this.subscriber);
            this.eligInformation.sepFlag = false;
            this.eligInformation.sepEvent = '';
            this.eligInformation.sepEventDatetime = '';
            this.eligInformation.mailAddressLine1 = '';
            this.eligInformation.mailAddressLine2 = '';
            this.eligInformation.mailCity = '';
            this.eligInformation.mailState = '';
            this.eligInformation.mailZip = '';
            this.eligInformation.phoneNumber = '';
            this.eligInformation.emailAddress = '';
            this.eligInformation.eSignature = '';

        }
        return this.eligInformation;
    }



    /**
     * 
     * @summary saves eligibility information to db
     * @param eligibilityInformation information about the applicant(s) used for eligibility verification
     */
    public submitApplication(eligibilityInformation: models.EligibilityInformation, captchaResponse: string): Observable<any> {

        if (eligibilityInformation.sepEventDatetime) {
            var sepEventDateParts = eligibilityInformation.sepEventDatetime.split('-');
            if (sepEventDateParts.length == 1) {
                sepEventDateParts = eligibilityInformation.sepEventDatetime.split('/');
            }
            //please put attention to the month (parts[0]), Javascript counts months from 0:
            // January - 0, February - 1, etc
            var mySepEventDate = new Date(parseInt(sepEventDateParts[2]), parseInt(sepEventDateParts[0]) - 1, parseInt(sepEventDateParts[1]));
            eligibilityInformation.sepEventDatetime = mySepEventDate.toISOString().split('T')[0];

        }

        if (eligibilityInformation.sepEffectiveDatetime) {
            var sepEffectiveDateParts = eligibilityInformation.sepEffectiveDatetime.split('-');
            if (sepEffectiveDateParts.length == 1) {
                sepEffectiveDateParts = eligibilityInformation.sepEffectiveDatetime.split('/');
            }
            //please put attention to the month (parts[0]), Javascript counts months from 0:
            // January - 0, February - 1, etc
            var mySepEffectiveDate = new Date(parseInt(sepEffectiveDateParts[2]), parseInt(sepEffectiveDateParts[0]) - 1, parseInt(sepEffectiveDateParts[1]));
            eligibilityInformation.sepEffectiveDatetime = mySepEffectiveDate.toISOString().split('T')[0];

        }

        if (eligibilityInformation.phoneNumber) {
            let phoneNumber = eligibilityInformation.phoneNumber.replace(/-/g, "");
            phoneNumber = phoneNumber.replace(/ /g, "");
            phoneNumber = phoneNumber.replace(/\(/g, "");
            phoneNumber = phoneNumber.replace(/\)/g, "");
            eligibilityInformation.phoneNumber = phoneNumber;
        }


        eligibilityInformation.totalHouseholdMemberNumber = eligibilityInformation.applicants.length;


        for (var x = 0; x < eligibilityInformation.applicants.length; x++) {

            if (eligibilityInformation.applicants[0].federalIdNumber) {
                let fedIdNumber = eligibilityInformation.applicants[0].federalIdNumber.replace(/-/g, "")
                eligibilityInformation.applicants[0].federalIdNumber = fedIdNumber;
            }
            if (!eligibilityInformation.applicants[x].seekingCovgFlag) {
                delete eligibilityInformation.applicants[x].eligibleImmigrationStatusFlag;
                delete eligibilityInformation.applicants[x].medicareFlag;
                delete eligibilityInformation.applicants[x].medicaidFlag ;
                delete eligibilityInformation.applicants[x].accessToMinimumEssentialCovgFlag ;
                delete eligibilityInformation.applicants[x].differentMailingAddr ;
                delete eligibilityInformation.applicants[x].differentHomeAddr ;
                delete eligibilityInformation.applicants[x].incarcerationFlag ;
                delete eligibilityInformation.applicants[x].usCitizenFlag ;
                delete eligibilityInformation.applicants[x].employerOfferedCovgFlag ;
                delete eligibilityInformation.applicants[x].cHIPFlag ;
                delete eligibilityInformation.applicants[x].documentType ;
                delete eligibilityInformation.applicants[x].documentId;
            }
            if (eligibilityInformation.applicants[x].eligibleImmigrationStatusFlag == null ||
                eligibilityInformation.applicants[x].eligibleImmigrationStatusFlag == false ||
                eligibilityInformation.applicants[x].eligibleImmigrationStatusFlag == undefined) {
                delete eligibilityInformation.applicants[x].documentType ;
                delete eligibilityInformation.applicants[x].documentId ;
            }
            
            if (eligibilityInformation.applicants[x].income) {
                eligibilityInformation.applicants[x].income = parseInt(eligibilityInformation.applicants[x].income);
            }else{
                delete eligibilityInformation.applicants[x].income;
            }

            if (eligibilityInformation.applicants[x].dateOfBirth) {
                var parts = eligibilityInformation.applicants[x].dateOfBirth.split('-');
                if (parts.length == 1) {
                    parts = eligibilityInformation.applicants[x].dateOfBirth.split('/');
                }
                //please put attention to the month (parts[0]), Javascript counts months from 0:
                // January - 0, February - 1, etc
                var mydate = new Date(parseInt(parts[2]), parseInt(parts[0]) - 1, parseInt(parts[1]));
                eligibilityInformation.applicants[x].dateOfBirth = mydate.toISOString().split('T')[0];
            }
            if (!eligibilityInformation.applicants[x].differentHomeAddr) {
                eligibilityInformation.applicants[x].addressLine1 = eligibilityInformation.mailAddressLine1;
                eligibilityInformation.applicants[x].addressLine2 = eligibilityInformation.mailAddressLine2;
                eligibilityInformation.applicants[x].city = eligibilityInformation.mailCity;
                eligibilityInformation.applicants[x].state = eligibilityInformation.mailState;
                eligibilityInformation.applicants[x].zip = eligibilityInformation.mailZip;
            }
            delete eligibilityInformation.applicants[x].differentMailingAddr;
            delete eligibilityInformation.applicants[x].differentHomeAddr;
        }
        console.log(JSON.stringify(eligibilityInformation));
        var authHeaders = new Headers();
        authHeaders.append('recaptchaid', captchaResponse);
        authHeaders.append('content-type', 'application/json');
        return this.http.post(environment.eligInfoApiUrl, eligibilityInformation, { headers: authHeaders })
            .map((response: Response) => {
                if (response.status != 200) {
                    return undefined;
                } else {
                    this.confirmationNumber = response.json().confirmationNumber;
                    return response.json() || {};
                }
            });
    }

    public getConfirmationNumber() {
        return this.confirmationNumber;
    }
    public populateStates() {
        this.states = [];
        this.states.push(new Object({ value: "AL", description: "Alabama" }));
        this.states.push(new Object({ value: "AK", description: "Alaska" }));
        this.states.push(new Object({ value: "AZ", description: "Arizona" }));
        this.states.push(new Object({ value: "AR", description: "Arkansas" }));
        this.states.push(new Object({ value: "CA", description: "California" }));
        this.states.push(new Object({ value: "CO", description: "Colorado" }));
        this.states.push(new Object({ value: "CT", description: "Connecticut" }));
        this.states.push(new Object({ value: "DE", description: "Delaware" }));
        this.states.push(new Object({ value: "FL", description: "Florida" }));
        this.states.push(new Object({ value: "GA", description: "Georgia" }));
        this.states.push(new Object({ value: "HI", description: "Hawaii" }));
        this.states.push(new Object({ value: "ID", description: "Idaho" }));
        this.states.push(new Object({ value: "IL", description: "Illinois" }));
        this.states.push(new Object({ value: "IN", description: "Indiana" }));
        this.states.push(new Object({ value: "IA", description: "Iowa" }));
        this.states.push(new Object({ value: "KS", description: "Kansas" }));
        this.states.push(new Object({ value: "KY", description: "Kentucky" }));
        this.states.push(new Object({ value: "LA", description: "Louisiana" }));
        this.states.push(new Object({ value: "ME", description: "Maine" }));
        this.states.push(new Object({ value: "MD", description: "Maryland" }));
        this.states.push(new Object({ value: "MA", description: "Massachusetts" }));
        this.states.push(new Object({ value: "MI", description: "Michigan" }));
        this.states.push(new Object({ value: "MN", description: "Minnesota" }));
        this.states.push(new Object({ value: "MS", description: "Mississippi" }));
        this.states.push(new Object({ value: "MO", description: "Missouri" }));
        this.states.push(new Object({ value: "MT", description: "Montana" }));
        this.states.push(new Object({ value: "NE", description: "Nebraska" }));
        this.states.push(new Object({ value: "NV", description: "Nevada" }));
        this.states.push(new Object({ value: "NH", description: "New Hampshire" }));
        this.states.push(new Object({ value: "NJ", description: "New Jersey" }));
        this.states.push(new Object({ value: "NM", description: "New Mexico" }));
        this.states.push(new Object({ value: "NY", description: "New York" }));
        this.states.push(new Object({ value: "NC", description: "North Carolina" }));
        this.states.push(new Object({ value: "ND", description: "North Dakota" }));
        this.states.push(new Object({ value: "OH", description: "Ohio" }));
        this.states.push(new Object({ value: "OK", description: "Oklahoma" }));
        this.states.push(new Object({ value: "OR", description: "Oregon" }));
        this.states.push(new Object({ value: "PA", description: "Pennsylvania" }));
        this.states.push(new Object({ value: "RI", description: "Rhode Island" }));
        this.states.push(new Object({ value: "SC", description: "South Carolina" }));
        this.states.push(new Object({ value: "SD", description: "South Dakota" }));
        this.states.push(new Object({ value: "TN", description: "Tennessee" }));
        this.states.push(new Object({ value: "TX", description: "Texas" }));
        this.states.push(new Object({ value: "UT", description: "Utah" }));
        this.states.push(new Object({ value: "VT", description: "Vermont" }));
        this.states.push(new Object({ value: "VA", description: "Virginia" }));
        this.states.push(new Object({ value: "WA", description: "Washington" }));
        this.states.push(new Object({ value: "WV", description: "West Virginia" }));
        this.states.push(new Object({ value: "WI", description: "Wisconsin" }));
        this.states.push(new Object({ value: "WY", description: "Wyoming" }));
        return this.states;
    }
}
