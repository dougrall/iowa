// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  applicationUrl: '',
  envName: "local-hotfix",
  securityMode: 'local',
  jwtString: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZUJpbGxpbmcgLSBGbGV4IFBheSBhbmQgVmlldyAtIEdyb3VwO01lbWJlciBNYWludGVuYW5jZSAtIEdyb3VwO2VCaWxsaW5nIC0gRnVsbHkgSW5zdXJlZCBWaWV3IE9ubHkgLSBHcm91cDtlQmlsbGluZyAtIEZ1bGx5IEluc3VyZWQgUGF5IGFuZCBWaWV3IC0gR3JvdXA7RGVzaWduYXRlZCBTZWN1cml0eSBDb29yZGluYXRvciAtIEdyb3VwO2VCaWxsaW5nIC0gU2VsZiBGdW5kZWQgVmlldyBPbmx5IC0gR3JvdXA7RWxpZ2liaWxpdHkgVmVyaWZpY2F0aW9uIC0gR3JvdXA7V2ViIEJhc2VkIElucXVpcnkgLSBHcm91cDtlQmlsbGluZyAtIFNlbGYgRnVuZGVkIFBheSBhbmQgVmlldyAtIEdyb3VwO0NsYWltIFN0YXR1cyBJbnF1aXJ5IC0gR3JvdXA7TUFDIFJlcG9ydHMgLSBHcm91cDtlQmlsbGluZyAtIFNlbGYgRnVuZGVkIFBISSAtIEdyb3VwO1NlY3VyZSBFbWFpbCAtIEdyb3VwO01lbWJlciBNYWludGVuYW5jZSBSZXBvcnRpbmcgLSBHcm91cDtFbXBsb3llciBUb29sa2l0O2VCaWxsaW5nIC0gRmxleCBWaWV3IE9ubHkgLSBHcm91cDtlQmlsbGluZyAtIEZsZXggUEhJIC0gR3JvdXAiLCJ3aW5hY2NvdW50bmFtZSI6IkNISU5vUm9sZXMiLCJPcmdUeXBlIjoid3NHcm91cCIsIk9yZ2FuaXphdGlvbklkIjoiMTcxNSIsIm5iZiI6MTQ5MTQ5MzYzMiwiZXhwIjoxNDk5MjY5NjMyLCJpYXQiOjE0OTE0OTM2MzIsImlzcyI6IndlbGxtYXJrIiwiYXVkIjoid2VsbG1hcmsuY29tIn0.2OCvaRQ9a-POjuzlIJUzioG9lhFk3bP4yzIR51lXuok',
  wellmarkURL: 'https://wwwdev.extdev.wellmark.com/',
  eligInfoApiUrl: 'https://zf2j9aw5m6.execute-api.us-east-1.amazonaws.com/POC/eligibility-information',
  sepInfoAoiUrl: 'https://zf2j9aw5m6.execute-api.us-east-1.amazonaws.com/POC/sep-information'
};
