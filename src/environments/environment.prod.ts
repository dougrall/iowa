export const environment = {
  production: true,
  securityMode: 'remote',
  jwtString: '',
  envName: 'production',
  applicationUrl: '',
  wellmarkURL: 'https://www.wellmark.com/',
  eligInfoApiUrl: 'https://zf2j9aw5m6.execute-api.us-east-1.amazonaws.com/POC/eligibility-information',
  sepInfoAoiUrl: 'https://zf2j9aw5m6.execute-api.us-east-1.amazonaws.com/POC/sep-information',
  recaptchaId:'6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
};
