// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  envName: "dev",
  securityMode: 'remote',
  jwtString: '',
  applicationUrl: '',
  wellmarkURL: 'https://wwwdev.extdev.wellmark.com/',
  eligBenefitsApiUrl: 'http://vdiwin764pdv194:8081/elig-verification/v1/',
  eligInfoApiUrl: 'https://zf2j9aw5m6.execute-api.us-east-1.amazonaws.com/POC/eligibility-information',
  sepInfoAoiUrl: 'https://zf2j9aw5m6.execute-api.us-east-1.amazonaws.com/POC/sep-information',
  recaptchaId:'6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
};
