// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  applicationUrl: '',
  envName: "local",
  securityMode: 'local',
  jwtString: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZUJpbGxpbmcgLSBGbGV4IFBheSBhbmQgVmlldyAtIEdyb3VwO01lbWJlciBNYWludGVuYW5jZSAtIEdyb3VwO2VCaWxsaW5nIC0gRnVsbHkgSW5zdXJlZCBWaWV3IE9ubHkgLSBHcm91cDtlQmlsbGluZyAtIEZ1bGx5IEluc3VyZWQgUGF5IGFuZCBWaWV3IC0gR3JvdXA7RGVzaWduYXRlZCBTZWN1cml0eSBDb29yZGluYXRvciAtIEdyb3VwO2VCaWxsaW5nIC0gU2VsZiBGdW5kZWQgVmlldyBPbmx5IC0gR3JvdXA7RWxpZ2liaWxpdHkgVmVyaWZpY2F0aW9uIC0gR3JvdXA7V2ViIEJhc2VkIElucXVpcnkgLSBHcm91cDtlQmlsbGluZyAtIFNlbGYgRnVuZGVkIFBheSBhbmQgVmlldyAtIEdyb3VwO0NsYWltIFN0YXR1cyBJbnF1aXJ5IC0gR3JvdXA7TUFDIFJlcG9ydHMgLSBHcm91cDtlQmlsbGluZyAtIFNlbGYgRnVuZGVkIFBISSAtIEdyb3VwO1NlY3VyZSBFbWFpbCAtIEdyb3VwO01lbWJlciBNYWludGVuYW5jZSBSZXBvcnRpbmcgLSBHcm91cDtlQmlsbGluZyAtIEZsZXggVmlldyBPbmx5IC0gR3JvdXA7ZUJpbGxpbmcgLSBGbGV4IFBISSAtIEdyb3VwO0JsdWUgSW5zaWdodHMgU0YgVXNlciIsIndpbmFjY291bnRuYW1lIjoiQ0hJTm9Sb2xlcyIsIk9yZ1R5cGUiOiJ3c0dyb3VwIiwiT3JnYW5pemF0aW9uSWQiOiIxNzE1IiwiQWNjb3VudEtleSI6IjAwMDM4NDcxIiwibmJmIjoxNDk3NjIxNjU5LCJleHAiOjE1MDUzOTc2NTksImlhdCI6MTQ5NzYyMTY1OSwiaXNzIjoid2VsbG1hcmsiLCJhdWQiOiJ3ZWxsbWFyay5jb20ifQ.10koWcR7a0vcuEuLFcue022E2uuSteoGMHm-ezZoDGI',
  wellmarkURL: 'https://wwwdev.extdev.wellmark.com/',
  eligInfoApiUrl: 'http://vdiwin764pdv194.int.wellmark.com:3003/eligibility/api/v1/application',
  sepInfoAoiUrl: 'https://zf2j9aw5m6.execute-api.us-east-1.amazonaws.com/POC/sep-information',
  recaptchaId:'6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
};
