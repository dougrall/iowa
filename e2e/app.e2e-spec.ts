import { MemberPortalPage } from './app.po';

describe('member-portal App', () => {
  let page: MemberPortalPage;

  beforeEach(() => {
    page = new MemberPortalPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
